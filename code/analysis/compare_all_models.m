
dataDir = '../../data/'
figDir = '../../figures/'
%models = {'oriented-cm-formlets',  'oriented-cm-regularized-formlets'};
%models = {'isotropic-formlets', 'oriented-formlets'};
models = {'isotropic-formlets', 'oriented-formlets', 'oriented-cm-formlets',  'oriented-cm-regularized-formlets'};
%occlusion = '10%';
occlusion = '30%';

addpath('../Pursuit/');


%% load data and compute error and
%if ~exist('comp', 'var') & ~exist([dataDir 'compare-all-formlet-models-' occlusion '-occlusion.mat'], 'file') 
  for m=1:length(models)
    load([dataDir, occlusion, '-occlusion-', models{m}]);
%     if m==1 
%       [~, comp(m).mean_err, comp(m).se_err] = Find_Mean_l2_Nonparm_Error_Shapelets(shapelet_data);      
%     else
    [comp(m).err, comp(m).mean_err, comp(m).se_err] = find_mean_l2_nonparm_error(formlet_data);    
%     end
    comp(m).model = models{m};
  end
  save([dataDir 'compare-all-formlet-models-' occlusion '-occlusion'], 'comp');
%end

%% plot reconstruction error
load([dataDir 'compare-all-formlet-models-' occlusion '-occlusion'])
colors = linspecer(4);
nit=32; 
clf
for m=1:length(models)
  hvis(m)=  plot(1:nit, comp(m).mean_err.formletRep.visible, 'color', colors(m,:), 'LineWidth', 2); hold on;
  hocc(m) = plot(1:nit, comp(m).mean_err.formletRep.occluded, '--', 'color', colors(m,:), 'LineWidth', 2); hold on;
end
legend(hvis, models);
%h2= plot(1:nit, comp(2).mean_err.formletRep.visible, 'color', colors(2,:), 'LineWidth', 2); hold on;
%legend([hvis, hocc], [models{1} ' (unoccluded)'] , [models{2} ' (unoccluded)'], ...
%    [models{1} ' (occluded)'] , [models{2} ' (unoccluded)']);

%error bars
% for m=1:2
%   errorbar(1:nit,comp(m).mean_err.formletRep.visible, comp(m).se_err.formletRep.visible, '.k', 'color', colors(m,:));  
% end

xlabel('Number of Components');
ylabel('Normalized RMS Error');
box off;

set(findall(gcf,'Type','text'),'FontSize',14)  
%set(findall(gcf,'Type','text'),'FontSize',28) %poster
saveas(gcf, [figDir 'convergence-regularization-' occlusion '-occlusion.eps'], 'epsc');
title([occlusion ' occlusion'])
%saveas(gcf, [figDir 'convergence-regularization-' occlusion '-occlusion.png'], 'png');
saveas(gcf, [figDir 'convergence-all-models-' occlusion '-occlusion.png'], 'png');

%% regularization now seems to increase completion error. Possible reasons:
%  [ ] regularized model is not being trained
%  [ ] changed dataset (252 animal, removed duplicates)
%  [ ] randomized occlusion fraction -> less bias. previous result due to
%  noise
%  [no] error in previous result 
%  [ ] fixed bug in  reparameterization (?)