clear all;

dataDir = '../../data/';
figRootDir = '../../figures/';
%figRootDir = '~/dump/';
%figRootDir= '~/Dropbox/Thesis/formlets-release/figures/';
addpath('/Users/alex/Dropbox/Thesis/formlets-release/code/pursuit');

%models = {'shapelets', 'isotropic-formlets', 'oriented-formlets', 'oriented-cm-formlets', 'oriented-cm-regularized-formlets'};
%models = {'shapelets', 'isotropic-formlets'};
models = {'shapelets', 'isotropic-formlets', 'oriented-formlets'};
M = length(models);

%occlusion = '0%';
occlusion = '10%';
%occlusion = '30%';

addpath('../Pursuit/');

dataDir = '~/Dropbox/Thesis/formlets-release/data/';

showGrid=1;
showRes=0;

colors = linspecer(length(models)); 
colors(1,:) = [0.1 0.1 .8] %make blue darker so it's distinguishable from gray

for shape=67%2%1:10 %179
        
    
    for m = 1:M
      figDir{m} = [figRootDir, num2str(shape), '/', occlusion, '-occlusion/', models{m}, '/'];
      mkdir(figDir{m}); 
    end
          
    
    %% CM
    
    % models = {'CM', 'Baseline'};
    % dataDir = '../../Data/no-occlusion/';
    % figRootDir= '~/Dropbox/Thesis/Documentation/thesis-manuscript/cm/fig/pursuit/';
    % figDir = {[figRootDir, 'cm/'], [figRootDir, 'no-cm/']};
    % fileNames = {'formlets-ellipse-cm', 'formlets-ellipse-no-cm'};
    %
    % %plotting preferences
    % shape = 1;
    % showGrid=0;
    % showRes=1;      
            
    %% plot iterations
    for m = 2:M
               
        load([dataDir, occlusion, '-occlusion-', models{m}, '-optimal-ellipse.mat']);
                        
        if m == 1 %shapelets
            formlet_data = shapelet_data;
            showGrid = 0;            
        end
        
        grid = plotter(formlet_data{shape}.target, formlet_data{shape}.formlets.init, [], showRes, showGrid, [], [], colors(m,:));
        %grid = plotter(formlet_data{shape}.target, formlet_data{shape}.formlets.formletRepAfterRepam{1}, [], showRes, showGrid, [], [], colors(m,:));
        
        
        it0_axis = axis; %to keep the same axis at
        axis(it0_axis);
        saveas(gcf, [figDir{m}, 'it00'], 'epsc');
        
        it0_axis = axis;
        for el = 1:32%1:32
            plotter(formlet_data{shape}.target, formlet_data{shape}.formlets.formletRep{el}, formlet_data{shape}.formlets.element{el}, showRes, showGrid, [],[], colors(m,:));
            axis(it0_axis);
            saveas(gcf, [figDir{m} sprintf('it%0.2d', el)], 'epsc');
        end
        
        
        %% show composition of formlets applied to a grid        
            grid = plotter(formlet_data{shape}.target, formlet_data{shape}.formlets.init, [], 0, showGrid, [], colors(m,:));
            for el = 1:32
                grid = plotter(formlet_data{shape}.target, formlet_data{shape}.formlets.formletRep{el}, formlet_data{shape}.formlets.element{el}, showRes, showGrid, [], [], colors(m,:), grid);
            end
            plotter(formlet_data{shape}.target, formlet_data{shape}.formlets.formletRep{el}, formlet_data{shape}.formlets.element{el}, showRes, showGrid, [], [], colors(m,:), grid);
            
            saveas(gcf, [figDir{m}, 'formlet_composition.eps'], 'epsc');
            axis(it0_axis);
            saveas(gcf, [figDir{m}, 'formlet_composition_imaxis.eps'], 'epsc');        
    end
    
    
end