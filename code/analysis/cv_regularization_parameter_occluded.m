
%% load data
lambdas = [0, linspace(0.0002, 0.006, 16)];
ntrain = '64';
dataDir = '~/Dropbox/Thesis/formlets-release/data/cross-validation/';
figDir = ['~/Dropbox/Thesis/formlets-release/figures/cross-validation/'];

nlambdas = length(lambdas);

for i=1:nlambdas
  load([dataDir, 'train-64-formlets-lambda-' num2str(lambdas(i)) '.mat']);        
   compCV(i).lambda = lambdas(i);
   compCV(i).pref= pref;
   compCV(i).data = formlet_data;
   compCV(i).l2err = find_mean_l2_nonparm_error(formlet_data); %get l2 error and run time  
   %[compCV(i).energy, compCV(i).mean_log_energy, compCV(i).se_log_energy] = Find_Energy(formlet_data); %get correlation
end

save([dataDir, 'formlet-model-comparison-cv-occluded' ntrain '.mat'], 'compCV', '-v7.3');
%% plot l2 error

load([dataDir, 'formlet-model-comparison-cv-occluded' ntrain '.mat']);

niterations = compCV(2).pref.niterations;
 
colors = linspecer(nlambdas); %define color map

%figure; hold on;
clf; hold on;
for i = 1:nlambdas
    plot(1:niterations, mean(compCV(i).l2err.visible), 'color', colors(i,:));     
    plot(1:niterations, mean(compCV(i).l2err.occluded), '--', 'color', colors(i,:));     
end

%plot(1:niterations, comp(1).l2err.formletRep.visible, 'color', colors(1,:),  'LineWidth', 2);
%plot(1:niterations, comp(4).l2err.formletRep.visible, 'color', [1 .3 .3],  'LineWidth', 2);
plot(1:niterations, compCV(22).l2err.formletRep.visible, 'color', [1 0 0],  'LineWidth', 2);

upperBound = compCV(1).l2err.formletRep.visible(1:niterations-1) 
plot(2:niterations, upperBound, 'color', 'black', 'LineWidth', 4) 

for i =1:1nlambdas
  satisfiesConstraint(i) = sum(compCV(i).l2err.formletRep.visible < [inf, upperBound])==32  
end
lambdaOpt = lambdas(max(find(satisfiesConstraint)))

%i=4; plot(1:niterations, comp(i).l2err.formletRep.visible, 'color', 'blue',  'LineWidth', 2);
colormap(colors);
colorbar('YTickLabel', lambdas);


axisBounds = axis;
axis([0, niterations+1, axisBounds(3:4)]);

xlabel('Number of Components');
ylabel('Normalized RMS Error');
set(findall(gcf,'Type','text'),'FontSize',16)
%title 'Error function for cross-validation of regularization parameter';
saveas(gcf, [figDir, 'error_function_cv' ntrain '.eps'], 'epsc');

%% plot log energy
figure; hold on;
for i = 2:nlambdas
    mean_log_energy{i} = compCV(i).mean_log_energy;
    se_log_energy{i} = compCV(i).se_log_energy;
    plot(mean_log_energy{i}, 'LineWidth', 2, 'color', colors(i,:));
    %errorbar(1:niterations,mean_log_energy{i}, se_log_energy{i}, se_log_energy{i},  '.k', 'color', colors(i,:));
end
 plot(mean_log_energy{4}, 'LineWidth', 2, 'color', [1 .3 .3])
xlabel 'Iteration';
ylabel 'Log energy';
set(findall(gcf,'Type','text'),'FontSize',16)

saveas(gcf, [figDir 'log-energy-cv' ntrain '-1curve.eps'], 'epsc');