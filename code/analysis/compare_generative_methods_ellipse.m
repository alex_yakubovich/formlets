dataDir = '../../data/'
%figDir = '../../figures/'

init = 'old-ellipse';

figDir = '~/Dropbox/Thesis/Documentation/thesis-manuscript/anisotropic/fig/'
models = {'shapelets', 'isotropic-formlets', 'oriented-formlets'};

%figDir = '~/Dropbox/Thesis/Documentation/thesis-manuscript/cm/fig/'
%models = {'shapelets', 'isotropic-formlets', 'oriented-formlets', 'oriented-cm-formlets'}

%figDir = '~/Dropbox/Thesis/Documentation/thesis-manuscript/regularization/fig/'
%init = 'new-ellipse';
%models = {'shapelets', 'isotropic-formlets', 'oriented-formlets', 'oriented-cm-formlets', 'oriented-cm-regularized-formlets'};

occlusion = '0%';
%occlusion = '10%';
%occlusion = '30%';
%occlusion = '50%';
addpath('../Pursuit/');


%% load raw data and compute l2 error if it hasn't been computed previously
if ~exist('comp', 'var') & ~exist([dataDir 'compare-all-formlet-models-' occlusion '-occlusion-old-ellipse.mat'], 'file') 
%if ~exist('comp', 'var') & ~exist([dataDir 'compare-all-formlet-models-' occlusion '-occlusion-old-ellipse.mat'], 'file') 
  for m=1:length(models)
    load([dataDir, occlusion, '-occlusion-', models{m}, '-' init]);
     if m==1 
       [comp(m).err, comp(m).mean_err, comp(m).se_err] = find_mean_l2_nonparm_error_shapelets(shapelet_data);      
     else
     [comp(m).err, comp(m).mean_err, comp(m).se_err] = find_mean_l2_nonparm_error(formlet_data);    
     end
    comp(m).model = models{m};
  end
  save([dataDir 'compare-all-formlet-models-' occlusion '-occlusion-' init], 'comp');
end

%% plot reconstruction error
load([dataDir 'compare-all-formlet-models-' occlusion '-occlusion-' init])
colors = linspecer(length(models)); 
nit=32; 
clf;

hvis = zeros(1, length(models));
hocc = zeros(1, length(models));

for m=1:length(models)
  hvis(m) =  plot(0:nit, [comp(m).mean_err.init.visible(1) comp(m).mean_err.formletRep.visible], 'color', colors(m,:), 'LineWidth', 2); hold on;
  %hvis(m)=  plot(1:nit, comp(m).mean_err.formletRep.visible, 'color', colors(m,:), 'LineWidth', 2); hold on;
  
  if str2num(occlusion(1)) > 0
      hocc(m) = plot(0:nit, [comp(m).mean_err.init.occluded(1) comp(m).mean_err.formletRep.occluded], '--', 'color', colors(m,:), 'LineWidth', 2); hold on;
      %hocc(m) = plot(1:nit, comp(m).mean_err.formletRep.occluded, '--', 'color', colors(m,:), 'LineWidth', 2); hold on;
  end
end
legend(hvis, models);
%h2= plot(1:nit, comp(2).mean_err.formletRep.visible, 'color', colors(2,:), 'LineWidth', 2); hold on;
%legend([hvis, hocc], [models{1} ' (unoccluded)'] , [models{2} ' (unoccluded)'], ...
%    [models{1} ' (occluded)'] , [models{2} ' (unoccluded)']);

%error bars
% for m=1:2
%   errorbar(1:nit,comp(m).mean_err.formletRep.visible, comp(m).se_err.formletRep.visible, '.k', 'color', colors(m,:));  
% end

xlabel('Number of Components');
ylabel('Normalized RMS Error');
box off;

%set(findall(gcf,'Type','text'),'FontSize',14)  
set(gca, 'FontSize', 16);
set(findall(gcf,'Type','text'),'FontSize',16)  

%set(findall(gcf,'Type','text'),'FontSize',28) %poster
%title([occlusion ' occlusion'])
%saveas(gcf, [figDir 'convergence-regularization-' occlusion '-occlusion.png'], 'png');
saveas(gcf, [figDir 'convergence-all-models-' occlusion(1) '-percent-occlusion-' init], 'epsc');
