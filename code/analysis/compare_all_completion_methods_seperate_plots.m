
%dataDirGenerativeModels = '~/Dropbox/Thesis/Regularization_Experiment/Data/10%-occlusion/'
dataDirGenerativeModels = '~/Dropbox/Thesis/formlets-release/data/'
dataDirBasicModels = '~/Dropbox/Thesis/Compare_Completion_Models/Data/'

figDir = '~/dropbox/Thesis/Documentation/thesis-manuscript/completion/fig/';
%figDir = '../../figures/';
models = {'shapelets', 'isotropic-formlets', 'oriented-formlets', 'oriented-cm-formlets', 'oriented-cm-regularized-formlets', 'elastica', 'linear-interpolation'};

M = length(models);

%occlusions = [.1, .3];
occlusions = [.1, .3, .5];

clf;

for o = 1:length(occlusions)
    
    occlusion = occlusions(o);        
    
    occlusion_percent = [num2str(100*occlusion) '%'];
    %occlusion = '10%';
    %occlusion = '30%';
    
    load([dataDirGenerativeModels 'compare-all-formlet-models-' occlusion_percent '-occlusion-optimal-ellipse'], 'comp');
    load([dataDirBasicModels occlusion_percent '-linear-elastica-error-image-coordinates-lambda-2.5714.mat']);
    %load([dataDirBasicModels occlusion_percent '-linear-elastica-error-image-coordinates.mat']);
    %load([dataDirBasicModels 'linear_elastica_completions.mat']);
    
    colors = linspecer(length(models));
    
    %% plot reconstruction error
    nit=32;
    cla;
    %figure;
    
    %subplot(1,length(occlusions)+1,o);
    for m=1:length(models)-2
        h(m) = plot(0:nit, [comp(m).mean_err.init.occluded(1) comp(m).mean_err.formletRep.occluded], 'color', colors(m,:), 'LineWidth', 2); hold on;
    end
    h(m+1) = plot(0:nit, elastica_err * ones(1,nit+1),'color', colors(m+1,:), 'LineWidth', 2); hold on;
    h(m+2) = plot(0:nit, linear_err * ones(1,nit+1),'color', colors(m+2,:), 'LineWidth', 2); hold on;
    
    %if o==length(occlusions)
        legend(h, models, -1);
        legend boxoff;
    %end
    %legend(h, models, 'Location', 'NorthEast');
    
    box off
    xlabel('Number of Components');
    ylabel('RMS Error');
    title([num2str(occlusion*100) '% occlusion'])
    %box off;
    set(findall(gcf,'Type','text'),'FontSize',10)       
    set(gca, 'FontSize', 10);
    
    axis image
    x = axis;
    x(3) = 0.95* x(3);
    x(4) = 1.05 * x(4);
    axis(x);
    %axis([0 32, 15, 70]);
    %axis([0 32, 15, 120]);
    %saveas(gcf,[figDir num2str(100*occlusion) '-percent-contour-completion-comparison'], 'epsc')
    saveas(gcf,[figDir num2str(100*occlusion) '-percent-contour-completion-comparison-elastica-cv'], 'epsc')
end
%saveas(gcf,[figDir 'contour-completion-comparison'], 'epsc')
%%%%%%%%%%


