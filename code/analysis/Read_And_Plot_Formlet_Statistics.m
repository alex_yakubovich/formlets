%Read the data from the formlet pursuit experiment, compute the mean
%formlet parameters at each iteration and plot their histogram

%See also Batch_Occluded_Pursuit, Formlet Pursuit
close all;

%dataDir = '../../Data/';
%figDir = '../../../Documentation/Proposal/regularization/fig/';

dataDir = '../../Data/10%-occlusion/'
%figDir = '~/Dropbox/Thesis/Documentation/thesis-manuscript/regularization/fig/';
%figDir = '/Users/alexyakubovich/dropbox/Thesis/Documentation/thesis-manuscript/fig/regularization/';
%figDir = '/Users/alexyakubovich/Dropbox/Thesis/Documentation/cvpr2014/fig/regularization/'
figDir = '~/dropbox/Thesis/Documentation/crv2014/fig/regularization/';

%figDir = '~/dump/'
occlusion = '0.1';
%fnames = {'oriented-formlets-regularized-10%-occlusion.mat', 'oriented-formlets-unregularized-10%-occlusion.mat'};
%models = {'regularized', 'unregularized'};

fnames = {'oriented-formlets-unregularized-10%-occlusion.mat', 'oriented-formlets-regularized-10%-occlusion.mat'};
models = {'Unregularized', 'Regularized'};

colors = linspecer(2);
colors = [colors(2,:); colors(1,:)]
%colors{1} = [1 .3 .3]; %red
%colors{2} = [16 78 139]/255; %dark blue
%lstyles = {'-', '--'};
lstyles = {'-', '-'};
%lwidths = [2 1];
lwidths= [2 2 ];

for m = 1:2
    load([dataDir, fnames{m}])
    %load([dataDir 'Formlet_Pursuit_Files_1to391_' fnames{m}]);
    % read formlet data
    data = formlet_data;
    
    nElements = numel(data{1}.formlets.element);
    nImages = numel(data);
    its = 1:nElements;
    
    zvals{m} = zeros(nImages, nElements);
    avals{m} = zeros(nImages, nElements);
    svals{m} = zeros(nImages, nElements);
    kvals{m} = zeros(nImages, nElements);
    thetavals{m} = zeros(nImages, nElements);
    
    for i=1:nImages
        for j=1:nElements
            avals{m}(i,j) = data{i}.formlets.element{j}.a;
            svals{m}(i,j) = data{i}.formlets.element{j}.s;
            zvals{m}(i,j) = data{i}.formlets.element{j}.z;
            kvals{m}(i,j) = data{i}.formlets.element{j}.k;
            thetavals{m}(i,j) = data{i}.formlets.element{j}.theta;
        end
    end
end

colorsSimple = ['r', 'b'];

%scale histogram
 %figure;
 clf;
  for m =1:2
     smeans{m} = mean(log(svals{m}));
     stdErr{m} = std(log(svals{m}))./sqrt(391);
     figh{m} = plot(smeans{m}, lstyles{m}, 'LineWidth', lwidths(m), 'color', colors(m,:));
     hold on
     %errorbar(its, smeans{m}, stdErr{m}, stdErr{m},'.', 'Color', colors(m,:));     
     shadedErrorBar(1:32,mean(log(svals{m})),std(log(svals{m}))/sqrt(nImages),['-', colorsSimple(m)]);
  end
  
  legend([figh{1}, figh{2}], models{1} , models{2}, 'Location', 'NorthEast');
 xlabel 'Iteration';
 ylabel 'Mean log scale'
 box off
 
 legend boxoff;
 set(gca, 'FontSize', 34);
 set(findall(gcf,'Type','text'),'FontSize',34)
%  set(findall(gcf,'Type','text'),'FontSize',20)
 saveas(gcf,[figDir 'mean-scale10'], 'epsc');
 
  
 %(absolute) gain histogram
 cla; 
  for m =1:2
     %ameans{m} = mean(abs(log(avals{m})));
     %stdErr{m} = std(log(avals{m}))./sqrt(391);
     ameans{m} = mean(log(abs(avals{m})));
     stdErr{m} = std(log(abs(avals{m})))./sqrt(391);
     shadedErrorBar(1:32,mean(log(abs(avals{m}))),std(log(abs(avals{m})))/sqrt(nImages),['-', colorsSimple(m)]);
     figh{m} = plot(ameans{m}, lstyles{m}, 'LineWidth', lwidths(m), 'color', colors(m,:));
     hold on
     %errorbar(its, ameans{m}, stdErr{m}, stdErr{m},'.', 'Color', colors(m,:));     
 end
 legend([figh{1}, figh{2}], models{1} , models{2}, 'Location', 'NorthEast');
 xlabel 'Iteration';
 ylabel 'Mean log absolute gain';
 %ylabel 'Mean absolute log gain';
   %set(findall(gcf,'Type','text'),'FontSize',20)
   set(gca, 'FontSize', 34);
 set(findall(gcf,'Type','text'),'FontSize',34);
 legend boxoff;
   box off
saveas(gcf,[figDir 'mean-absolute-gain10'], 'epsc');

 
 % alpha/sigma
%  clf;
%  asmeans = mean(abs(avals)./svals);
%  stdErr = std(svals)./sqrt(391);
%  errorbar(its, asmeans, stdErr, stdErr ,'.');
%  xlabel 'Formlet number $k$';
%  ylabel 'Mean abs. gain:scale $(|\alpha| / \sigma)$'
%  matlabfrag([figDir 'figures/Marginal_Distributions/mean_gain_over_scale_hybrid_gabor']);
 
 %location histogram
 clf;
 histData = hist3([real(zvals(:)), imag(zvals(:))], 'nbins',[750,750]);
 x = linspace(min(real(zvals(:))),max(real(zvals(:))), size(histData,1)+1);
 y = linspace(min(imag(zvals(:))),max(imag(zvals(:))), size(histData,1)+1);
 histData1 = histData';
 histData1(size(histData,1) + 1 ,size(histData,2) + 1 ) = 0;
 pcolor(x,y, histData1);
 axis([-.4, .4, -.4, .4])
 xlabel '${\bf Re} (\zeta)$';
 ylabel '${\bf Im} (\zeta)$';
 matlabfrag([figDir 'Marginal_Distributions/pooled_space_hybrid_gabor']); 
 
 %concentration histogram
 %figure;
 clf;
 kmeans = mean(kvals);
 stdErr = std(kvals)./sqrt(391);
 errorbar(its, kmeans, stdErr, stdErr ,'.');
 xlabel 'Formlet number $k$';
 ylabel 'Mean scale ($\sigma$)'
 matlabfrag([figDir 'figures/Marginal_Distributions/mean_scale_hybrid_gabor']);