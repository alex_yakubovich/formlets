
function [err, mean_err, se] = find_mean_l2_nonparm_error_shapelets(shapelet_data)

nimages = length(shapelet_data);
nelements = numel(shapelet_data{1}.shapelets.element);

for n = 1:nimages
  
  trial = shapelet_data{n};
  
  %trial.shapelets.init = trial.ellipse;
  trial.shapelets.init = trial.init;
  
  
  for c = 1:nelements
    
    init.visible(n,c) = l2nonparam([real(trial.target.visible) imag(trial.target.visible)], [real(trial.shapelets.init.visible) imag(trial.shapelets.init.visible)]);
    formletRep.visible(n,c) = l2nonparam([real(trial.target.visible) imag(trial.target.visible)], [real(trial.shapelets.visible{c}) imag(trial.shapelets.visible{c})]);
    
    
    if ~isempty(trial.target.occluded)
        init.occluded(n,c) = l2nonparam([real(trial.target.occluded) imag(trial.target.occluded)], [real(trial.shapelets.init.occluded) imag(trial.shapelets.init.occluded)]);
        formletRep.occluded(n,c) = l2nonparam([real(trial.target.occluded) imag(trial.target.occluded)], [real(trial.shapelets.occluded{c}) imag(trial.shapelets.occluded{c})]);
    end
    
  end
  disp(['computed l2error for image : ' num2str(n) '/' num2str(nimages)]);
end



err = formletRep;

%mean err on visible portion
mean_err.init.visible = mean(init.visible,1);
mean_err.formletRep.visible = mean(formletRep.visible,1);

%mean err on occluded portion
if ~isempty(trial.target.occluded) %check for occlusion
  mean_err.init.occluded = mean(init.occluded,1);
  mean_err.formletRep.occluded = mean(formletRep.occluded,1);  
end

%standard errors
se.init.visible = std(init.visible,1)/sqrt(nimages);
se.formletRep.visible = std(formletRep.visible,1)/sqrt(nimages);

if ~isempty(trial.target.occluded) %check for occlusion
  se.init.occluded = std(init.occluded,1)/sqrt(nimages);
  se.formletRep.occluded = std(formletRep.occluded,1)/sqrt(nimages);  
end

%% run time
 for n = 1:nimages
     time(n) = shapelet_data{n}.runningtime;
 end
 meanTime = mean(time);