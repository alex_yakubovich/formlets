clear all;

dataDir = '../../data/';

addpath('~/Dropbox/Thesis/formlets-release/code/pursuit');

figRootDir= '/Users/alex/Dropbox/Thesis/Documentation/thesis-manuscript/anisotropic/fig'

model = 'shapelets';

%occlusion = '0%';
occlusion = '10%';
%occlusion = '30%';

addpath('../Pursuit/');

dataDir = '~/Dropbox/Thesis/formlets-release/data/';

showGrid=1;
showRes=0;

load([dataDir, occlusion, '-occlusion-shapelets-optimal-ellipse.mat']);

m=1;
models={'shapelets'};
for shape=1:5:80%2%1:10 %179
    figDir{m} = [figRootDir, num2str(shape), '/', occlusion, '-occlusion/', models{m}, '/'];
    mkdir(figDir{m});
       
    load([dataDir, occlusion, '-occlusion-', models{m}, '-optimal-ellipse.mat']);    mkdir(figDir{m});
    
    grid = plotter(shapelet_data{shape}.target, shapelet_data{shape}.init, [], showRes, showGrid, [], [], 'blue');
    
    it0_axis = axis; %to keep the same axis at
    axis(it0_axis);
    saveas(gcf, [figDir{m}, 'it00'], 'epsc');
    
    it0_axis = axis;
    for el = 1:32%1:32
        
        shapeletRep = struct('visible', shapelet_data{shape}.shapelets.visible{el}, 'occluded', shapelet_data{shape}.shapelets.visible{el}, 'entire', [shapelet_data{shape}.shapelets.visible{el}; shapelet_data{shape}.shapelets.occluded{el}]);
        
        plotter(shapelet_data{shape}.target, shapelet_data{shape}.shapelets.formletRep{el}, shapelet_data{shape}.formlets.element{el}, showRes, showGrid, [],[], colors(m,:));
        
        axis(it0_axis);
        saveas(gcf, [figDir{m} sprintf('it%0.2d', el)], 'epsc');
    end
        
end






