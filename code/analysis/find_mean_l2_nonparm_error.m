
function [err, mean_err, se] = find_mean_l2_nonparm_error(formlet_data)
nimages = length(formlet_data);


if isfield(formlet_data{1}, 'shapelets')
  nelements = numel(formlet_data{1}.shapelets.element);
  
else
  nelements = numel(formlet_data{1}.formlets.element);
end

for n = 1:nimages
  
  trial = formlet_data{n};
  
  
  for c = 1:nelements 

    %init.visible(n,c) = l2nonparam([real(trial.target.visible) imag(trial.target.visible)], [real(trial.formlets.init.visible) imag(trial.formlets.init.visible)]);       
   
    formletRep.visible(n,c) = l2nonparam([real(trial.target.visible) imag(trial.target.visible)], [real(trial.formlets.formletRep{c}.visible) imag(trial.formlets.formletRep{c}.visible)]);
           
    if ~isempty(trial.target.occluded)
     % init.occluded(n,c) = l2nonparam([real(trial.target.occluded) imag(trial.target.occluded)], [real(trial.formlets.init.occluded) imag(trial.formlets.init.occluded)]);                  
      formletRep.occluded(n,c) = l2nonparam([real(trial.target.occluded) imag(trial.target.occluded)], [real(trial.formlets.formletRep{c}.occluded) imag(trial.formlets.formletRep{c}.occluded)]);
      
    end
  end
  disp(['computed l2error for image : ' num2str(n) '/' num2str(nimages)]);
end

err = formletRep;

%mean err on visible portion
%mean_err.init.visible = mean(init.visible,1);
mean_err.formletRep.visible = mean(formletRep.visible,1);

%standard errors
%se.init.visible = std(init.visible,1)/sqrt(nimages);
se.formletRep.visible = std(formletRep.visible,1)/sqrt(nimages);

if ~isempty(trial.target.occluded) %check for occlusion    
%  mean_err.init.occluded = mean(init.occluded,1);
  mean_err.formletRep.occluded = mean(formletRep.occluded,1);  
  
 % se.init.occluded = std(init.occluded,1)/sqrt(nimages);
  se.formletRep.occluded = std(formletRep.occluded,1)/sqrt(nimages);  
end

%% run time
 for n = 1:nimages
     time(n) = formlet_data{n}.runningtime;
 end
 meanTime = mean(time);
 