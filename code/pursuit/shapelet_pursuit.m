
function shapelet_data = shapelet_pursuit(pref, init)

%observations: get same convergence if rescale but start from normalized ellipse
%-> initialization has to be orthonormal?

if nargin==0; load('../../data/preferences.mat'); pref.init = 'ellipse-analytical-optimal'; end
%pref.coordinateSystem = 'unit-variance-in-each-dimension';
%pref.init = 'ellipse';
%pref.init = 'circle';

%prepare shapelet dictionary
shapelets.lambda = pref.bound*(1:pref.n)/pref.n;
shapelets.mu = (0:pref.n-1)/pref.n;
shapelets.sigma = pref.sigma;
shapelets.dictionary = zeros(length(shapelets.lambda), length(shapelets.mu), pref.n);
for lambda = 1:length(shapelets.lambda);
    for mu = 1:length(shapelets.mu);
        %save shapelet into dictionary
        shapelets.dictionary(lambda, mu, :) = shapelet_basis(shapelets.lambda(lambda), shapelets.mu(mu), shapelets.sigma, pref.n);        
    end    
end
clear('lambda', 'mu');

for file = 1:pref.nfiles
    
    fileName = pref.dataset.filelist(file).name; %get filename
    image = imread([pref.dataset.directory fileName]);
    
    %datum = extract_contour(image, pref.visibleSamples, pref.n, pref.offset(file), strcmp(pref.init, 'ellipse'));
    datum = extract_contour(image, pref.visibleSamples, pref.n, pref.offset(file), pref.coordinateSystem);
    datum.name = fileName;
    
    %[shapeletRep, datum] = fit_ellipse(datum, pref);
    
    if nargin == 2
        shapeletRep = init{file}
    else
        shapeletRep = initialize_pursuit(datum.target, datum.region, pref.visibleSamples, pref.n, pref.init, pref.optPair);
    end
    
    %shapeletRep = initialize_pursuit(datum.target.visible, datum.region, pref.visibleSamples, pref.n, pref.init, pref.optPair);
    
    % if ~strcmp(pref.coordinateSystem, 'unit-variance-in-each-dimension')
    %        [datum, shapeletRep] = normalize_shapes(shapeletRep, datum, 'unit-variance');
    %end
%         centroid = mean(datum.target.visible);
%         datum.target.entire = datum.target.entire - centroid;
%         datum.target.visible= datum.target.visible - centroid;
%         datum.target.occluded = datum.target.occluded - centroid;
%     
%         shapeletRep.entire = shapeletRep.entire - centroid;
%         shapeletRep.visible = shapeletRep.visible - centroid;
%         shapeletRep.occluded= shapeletRep.occluded- centroid;                       
   
   % shapeletRep = match_by_rotation(datum.target.visible, shapeletRep);
    
    datum.init = shapeletRep;
           
    
    for element = 1:pref.niterations        
        tic;                      
        
        %compute residual
        shapeletPursuit.residual = repmat(reshape(datum.target.visible - shapeletRep.visible, [1 1 pref.visibleSamples]), size(shapelets.dictionary,1), size(shapelets.dictionary,2));
        
        %compute optimal affine terms
        shapeletPursuit.affine{1,1} = sum(real(shapeletPursuit.residual).*real(shapelets.dictionary(:,:,1:pref.visibleSamples)),3);
        shapeletPursuit.affine{1,2} = sum(real(shapeletPursuit.residual).*imag(shapelets.dictionary(:,:,1:pref.visibleSamples)),3);
        shapeletPursuit.affine{2,1} = sum(imag(shapeletPursuit.residual).*real(shapelets.dictionary(:,:,1:pref.visibleSamples)),3);
        shapeletPursuit.affine{2,2} = sum(imag(shapeletPursuit.residual).*imag(shapelets.dictionary(:,:,1:pref.visibleSamples)),3);
        
        %find maximal energy affine transformation
        n = shapeletPursuit.affine{1,1}.^2 + shapeletPursuit.affine{1,2}.^2 + shapeletPursuit.affine{2,1}.^2 + shapeletPursuit.affine{2,1}.^2;
        [~, i1] = max(n);
        [~, i2] = max(max(n));
        i1 = i1(i2);
        shapeletPursuit.lambdaIndex = i1;
        shapeletPursuit.muIndex = i2;
        clear('n', 'm');
        
        %compute affine polynomial terms
        shapeletPursuit.A = 0.5*complex(shapeletPursuit.affine{1,1}(i1,i2) + shapeletPursuit.affine{2,2}(i1,i2), shapeletPursuit.affine{2,1}(i1,i2) - shapeletPursuit.affine{1,2}(i1,i2));
        shapeletPursuit.B = 0.5*complex(shapeletPursuit.affine{1,1}(i1,i2) - shapeletPursuit.affine{2,2}(i1,i2), shapeletPursuit.affine{1,2}(i1,i2) + shapeletPursuit.affine{2,1}(i1,i2));
        
        %transform optimal shapelet and add to representation
        shapeletPursuit.base = squeeze(shapelets.dictionary(i1,i2,:));
        shapeletPursuit.shapelet = shapeletPursuit.A*shapeletPursuit.base + shapeletPursuit.B*conj(shapeletPursuit.base);
        shapeletRep.visible = shapeletRep.visible + shapeletPursuit.shapelet(1:pref.visibleSamples);
        shapeletRep.occluded = shapeletRep.occluded + shapeletPursuit.shapelet(pref.visibleSamples+1:end);
        
        %save shapelet parameters
        shapelet.lambda = shapelets.lambda(i1);
        shapelet.mu = shapelets.mu(i2);
        shapelet.sigma = shapelets.sigma;
        shapelet.A = shapeletPursuit.A;
        shapelet.B = shapeletPursuit.B;
        shapelet.base = shapeletPursuit.base;
        shapelet.contour = shapeletPursuit.shapelet;
        clear('i1', 'i2');
        
        %store shapelet and corresponding contours into datum
        datum.shapelets.element{element} = shapelet;
        datum.shapelets.visible{element} = shapeletRep.visible;
        datum.shapelets.occluded{element} = shapeletRep.occluded;
        datum.shapelets.timer{element} = toc;
        
        plotter(datum.target,shapeletRep);
        axis on;
        drawnow
       % saveas(gcf, ['../../figures/shapelets/it' num2str(element)], 'epsc');
    end
    datum.runningtime = sum([datum.shapelets.timer{:}])/60;    
    shapelet_data{file} = datum; %store output
    
    
    %print a little message
    disp(['Processed ' datum.name ', file ' num2str(file) ' of ' num2str(pref.nfiles) ' in ' num2str(datum.runningtime) ' minutes.']);
    
    if pref.save
          save(pref.fileName, 'shapelet_data', 'pref');
    else
      display('Not saving data!');
    end     
end
