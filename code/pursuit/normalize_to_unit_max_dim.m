function [shapeletRep, datum, max_dim] = normalize_to_unit_max_dim(shapeletRep, datum)


n = length(shapeletRep.entire);


x = real(datum.contour);
    y = imag(datum.contour);    
    bR= repmat([x, y],1,n);
    MR=reshape([x'; y'], 1, 2*n); MR = repmat(MR, n, 1);
    dt=(MR-bR).^2; D1= sqrt(dt(:,1:2:end-1) + dt(:, 2:2:end) ); %  element(r,c) is the distance of rth point on a from cth point on b
    dt = abs(MR-bR);
    Dx = dt(:,1:2:end-1); %Dx[i,j] = |xi - xj |
    Dy = dt(:, 2:2:end); %Dy[i,j] = =|yi - yj| 
    
    max_dim = max(max(Dx(:)), max(Dy(:)));
    max_dim = max_dim;
    shapeletRep.entire = shapeletRep.entire/max_dim;
    shapeletRep.visible = shapeletRep.visible/max_dim;
    shapeletRep.occluded= shapeletRep.occluded/max_dim;
    
    
    
    
    datum.contour = datum.contour/max_dim; %normalize shape so that max(height,width) =1
    datum.target.entire = datum.target.entire/max_dim;
    datum.target.visible= datum.target.visible/max_dim;
    datum.target.occluded = datum.target.occluded/max_dim;