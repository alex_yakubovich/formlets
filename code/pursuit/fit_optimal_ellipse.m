function ellipse = fit_optimal_ellipse(shape, occludedSamples, method, niterations, viz)

% ellipse_final = fit_optimal_ellipse(shape) fits a least-squares ellipse
% to a shape (provided as a complex vector). We begin with a PCA ellipse,
% and then iterate between a reparameterization step and optimization step.

% ellipse_final = fit_optimal_ellipse(shape, occludedSamples) assume that
% occludedSamples are missing from the shape.

% ellipse_final = fit_optimal_ellipse(shape, .. , 'cyclic
% shifts') parameterizes the ellipse by applying the best cyclic shift.

% ellipse_final = fit_optimal_ellipse(shape, .., .., true) also plots each
% iteration

SetDefaultValue('occludedSamples',0);
SetDefaultValue('method', 'CM');
SetDefaultValue('viz',false);
SetDefaultValue('niterations',10);

figDir = '~/Dropbox/Thesis/Documentation/cvpr2014/followup/pursuit/optimal-ellipse/';

n = length(shape.entire);
visibleSamples = n - occludedSamples;

[~, ellipse_parameters] = fit_pca_ellipse(shape.visible);
ellipse = create_ellipse(ellipse_parameters);

if occludedSamples > 0
    [ellipse, ~] = match_by_rotation(shape.visible, ellipse);
else
    ellipse = struct('entire', ellipse, 'visible', ellipse(1:visibleSamples), 'occluded', ellipse(visibleSamples+1:end));
end


for i = 1:niterations
    
    if viz
        plotter(shape, ellipse, [], 1);
        %saveas(gcf, [figDir, sprintf('ellipse-%0.2i-C-affine-transformation.eps', i)], 'epsc');
    end
    
    %Step 1: Find best correspondence between the ellipse and the target shape
    ix = cm(shape.visible, ellipse.visible, occludedSamples == 0);
    ellipse.visible = reparameterize(ix, ellipse.visible);
    ellipse.entire = [ellipse.visible; ellipse.occluded];
    
    if viz;
        plotter(shape, ellipse, [], 1);
        %saveas(gcf, [figDir, sprintf('ellipse-%0.2i-B-optimize-correspondence.eps', i)], 'epsc');
    end
    
    %Step 2: Apply affine transformation minimizing l2 residual error
    if i < niterations        
        
        %find best affine transformation
        x_ellipse_vis=real(ellipse.visible); y_ellipse_vis=imag(ellipse.visible);
        x_target=real(shape.visible); y_target=imag(shape.visible);        
        [A,b] = find_affine_parameters(x_ellipse_vis, y_ellipse_vis, x_target, y_target);
        
        %apply it to the ellipse
        x_ellipse=real(ellipse.entire); y_ellipse=imag(ellipse.entire);
        ellipse = (A * [x_ellipse y_ellipse]' + repmat(b, [1, n]))';
        ellipse = complex(ellipse(:,1), ellipse(:,2));
        ellipse = struct('entire', ellipse, 'visible', ellipse(1:visibleSamples), 'occluded', ellipse(visibleSamples+1:end));
    end
    
end

if viz;
    plotter(shape, ellipse, [],1);
    saveas(gcf, [figDir, sprintf('ellipse-initialization.eps')], 'epsc');
end