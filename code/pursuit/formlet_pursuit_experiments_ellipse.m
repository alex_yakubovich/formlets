%variables in the formlet system
%   Basis: isotropic vs. anisotropic
%   Correspondence: cyclic-shifts vs. CM
%   Objective function: baseline vs. regularized

%pref = define_preferences(occlusion);
%save([dataDir 'preferences'], 'pref');

dataDir = '../../Data/';

for occlusion =  0.5 %[0.1, 0.3, 0]
    
    load([dataDir, 'offset-', num2str(occlusion*100), '.mat'], 'offset'); %load offset
    lambda = 0; %no regularization for first few experiments
    
    pref = define_preferences(occlusion, lambda, offset);        
    
    
    
    
    init = initialize_all_shapes(pref);    
    save(sprintf('init-%i%%-occlusion', 100*occlusion), 'init')
    
    %load(sprintf('init-%i%%-occlusion', 100*occlusion))
    
    pref.plot.iter = 1; %plot every iteration
    
    %pref.nfiles = 1; %run on one image to see if anything breaks
    %pref.save = 0; %don't overwrite data in this case
    
    %shapelets
    fprintf('\n \n Begin pursuit with shapelets at %d%% occlusion \n', occlusion * 100);
    pref.fileName = [dataDir, num2str(100*occlusion), '%-occlusion-shapelets-suboptimal-ellipse'];
    pref.optPair = 'cyclic-shifts';
   shapelet_pursuit(pref, init);
    
    % isotropic formlets
    fprintf('\n \n Begin pursuit with isotropic formlets \n')
    pref.fileName = [dataDir, num2str(100*occlusion), '%-occlusion-isotropic-formlets-suboptimal-ellipse'];
    pref.optPair = 'cyclic-shifts';
    pref.isotropic = 1;
    formlet_pursuit(pref, init);
    
    % oriented formlets
    fprintf('\n \n Begin pursuit with oriented formlets at %d%% occlusion \n', occlusion * 100);
    pref.fileName = [dataDir, num2str(100*occlusion), '%-occlusion-oriented-formlets-suboptimal-ellipse'];
    pref.optPair = 'cyclic-shifts';
    pref.isotropic = 0; %oriented
    formlet_pursuit(pref, init);
    
    %oriented formlets + cm
    fprintf('\n \n Begin pursuit with oriented formlets + reparameterization at %d%% occlusion \n', occlusion * 100);
    pref.fileName = [dataDir, num2str(100*occlusion), '%-occlusion-oriented-cm-formlets-suboptimal-ellipse'];
    pref.isotropic = 0; %oriented
    pref.optPair = 'CM'; %CM
    formlet_pursuit(pref, init);
    
    %oriented formlets + cm + regularization
    fprintf('\n \n Begin pursuit with oriented formlets + reparameterization + regularization at %d%% occlusion \n', occlusion * 100);
    pref.fileName = [dataDir, num2str(100*occlusion), '%-occlusion-oriented-cm-regularized-formlets-suboptimal-ellipse'];
    pref.isotropic = 0; %oriented
    pref.optPair = 'CM'; %CM
    pref.lambda = .003942; %regularization
    formlet_pursuit(pref, init);
    
end