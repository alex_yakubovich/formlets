
function L = Find_Fidelity_Term_Constant(testShape, formletRep,n)

% constants for objective function
avLen = sum((abs(testShape(2:n) - testShape(1:n-1)) + abs(formletRep(2:n) - formletRep(1:n-1))))/(2*n-2);

res_old = sum(abs(testShape - formletRep))/n; %average residual from previous iteration
%res_old = sum(abs(testShape - formletRep)); %total residual from previous

%L = avLen*res_old; %scaling factor for residual term
L = .5*avLen*res_old; %scaling factor for residual term
%L=1; 