function model= initialize_pursuit(shape, region, visibleSamples, totalSamples, method, correspondenceMethod)
%initialize_formlet_pursuit(shape, region, totalSamples,
%visibleSamples, method) provides a coarse approximation of shape using
%either a circle or an ellipse. region is a binary image of the region bounded by shape.

%initialize_formlet_pursuit(..., 'circle') returns the largest circle
%inscribed in shape.

%initialize_formlet_pursuit(..., 'ellipse-analytical') returns the ellipse minimizing
%l2 errors. Starting with a PCA ellipse, it iterates between
%a re-parameterization step and a (analytical) optimization step. 

%initialize_formlet_pursuit(..., 'ellipse-analytical-suboptimal') returns the ellipse minimizing
%l2 error with some constraints. This method is suboptimal because it does not search over the
%ellipse centre and assumes shapes have been normalized to unit variance in
%each direction'


if strcmp(method, 'circle')
    model = fit_circle_distance_transform(shape.visible, region, totalSamples, visibleSamples);
elseif strcmp(method, 'ellipse-analytical-suboptimal')
    model= fit_ellipse_analytically(shape.visible, totalSamples, visibleSamples);
elseif strcmp(method, 'ellipse-analytical-optimal')
    model = fit_optimal_ellipse(shape, totalSamples-visibleSamples, correspondenceMethod);
else
    fprintf('unknown initialization method selected! Select one of: circle, ellipse-analytical-optimal, ellipse-analytical-suboptimal');
end
end
function [circle, centre, radius] = fit_circle_distance_transform(shape, region, totalSamples, visibleSamples)
% Compute the maximal inscribed circle by taking the peak of the distance transform

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Distance Transform
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% linearly interpolate occluded part (if needed)
if totalSamples > visibleSamples
    t=linspace(0,1,totalSamples-visibleSamples)';
    interpolant = shape(end) + t*(shape(1) - shape(end)) ;
    targetClosed = [shape; interpolant];
    r = ceil(-imag(targetClosed));
    c = ceil(real(targetClosed));
    
else
    r = ceil(-imag(shape));
    c = ceil(real(shape));
end
boundaryImage = zeros(size(region));

%% compute binary image of the object boundary
for i = 1:totalSamples
    boundaryImage(r(i), c(i)) = 1;
end
boundaryImage = logical(boundaryImage);
[nrow, ncol] = size(boundaryImage);

%% compute interior of shape (if needed)
if totalSamples > visibleSamples
    [X,Y] = meshgrid(1:ncol, -nrow:-1);
    interior = inpolygon(X(:), Y(:), real(targetClosed), imag(targetClosed));
    interior = reshape(interior, nrow, ncol);
    interior = flipud(interior);
else
    interior = region;
end

%% compute distance transform
distanceTransform = bwdist(boundaryImage);
distanceTransform(~interior) = -realmax; %problem here
%gfilt = fspecial('gaussian', [50, 50], 5); %consider changing window, scale for gaussian filter
%DT_filt = imfilter(distanceTransform,gfilt);
%distanceTransform = DT_filt;

%% Fit Circle

%optimize circle parameters
[DTmax, max_i] = max(distanceTransform(:));
[r0,c0] = ind2sub(size(distanceTransform),max_i); %centre at circle at argmax DT[i,j]
Rpixels = DTmax;

%build  circle (image coordinate)
theta = (2*pi/totalSamples)*(1:totalSamples);
r_circ = Rpixels*cos(theta) + r0;
c_circ = Rpixels*sin(theta) + c0;

[x,y] = poly2ccw(c_circ, -r_circ); %orient counterclockwise
%[x,y] = poly2cw(c_circ, -r_circ); %orient clockwise

circle.entire = complex(x,y).';
circle.visible = circle.entire(1:visibleSamples);
circle.occluded= circle.entire(visibleSamples+1:totalSamples);

centre = [c0, - r0];
radius = Rpixels;
%% pick optimal occlude region (search across all rotations)

% don't need to do this (CM already searching across all rotations
% err = zeros(totalSamples,1);
% for s = 1:totalSamples
%     res= datum.target.visible - circshift(circle.visible,s);
%     err(s) = sum(IP(res,res));
% end
%
% [~, smin] = min(err);
% circle.entire = circshift(circle.entire,smin);
% circle.visible = circle.entire(1:visibleSamples);
% circle.occluded = circle.entire(visibleSamples+1:end);
end

function [ellipse] = fit_ellipse_analytically(shape, totalSamples, visibleSamples)

%given a target contour and the dictionary preferences, create an ellipse
%centered at the visible portion of the contour, with shape,scale and
%orientation selected to minimize l2 error.

ellipse.circle = exp(1i*((2*pi/totalSamples)*(1:totalSamples)))';
ellipse.circle = ellipse.circle - mean(ellipse.circle(1:visibleSamples));
ellipse.visible = ellipse.circle(1:visibleSamples);
ellipse.circle = complex(real(ellipse.circle)/sqrt(sum(real(ellipse.visible).^2)), imag(ellipse.circle)/sqrt(sum(imag(ellipse.visible).^2)));
ellipse.visible = complex(real(ellipse.visible)/sqrt(sum(real(ellipse.visible).^2)), imag(ellipse.visible)/sqrt(sum(imag(ellipse.visible).^2)));

%compute affine fit for ellipse
ellipse.affine = zeros(2,2);
ellipse.affine(1,1) = real(shape)' * real(ellipse.visible);
ellipse.affine(1,2) = real(shape)' * imag(ellipse.visible);
ellipse.affine(2,1) = imag(shape)' * real(ellipse.visible);
ellipse.affine(2,2) = imag(shape)' * imag(ellipse.visible);

%apply affine transformation
ellipse.A = 0.5*complex(ellipse.affine(1,1) + ellipse.affine(2,2), ellipse.affine(2,1) - ellipse.affine(1,2));
ellipse.B = 0.5*complex(ellipse.affine(1,1) - ellipse.affine(2,2), ellipse.affine(1,2) + ellipse.affine(2,1));

%overwrite visible and add occluded portion of affine-fit ellipse
ellipse.entire = ellipse.A*ellipse.circle + ellipse.B*conj(ellipse.circle);
%ellipse.entire = ellipse.entire + mean(shape);
ellipse.visible = ellipse.entire(1:visibleSamples);
ellipse.occluded = ellipse.entire(visibleSamples+1:end);
end
