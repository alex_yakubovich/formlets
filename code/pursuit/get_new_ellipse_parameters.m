function new_ellipse_parameters = get_new_ellipse_parameters(ellipse_parameters, A, bvec, viz)
% new_ellipse_parameters = get_new_ellipse_parameters(ellipse_parameters, A, bvec, viz)
% returns the parameters of an ellipse (x0,y0,a,b,theta) after it is subject to an affine transformation {A, bvec}

% get translation
SetDefaultValue('viz', 1);
SetDefaultValue('ellipse_parameters',struct('x0', 1, 'y0', 5, 'a',2,'b',5,'theta',3*pi/4))
SetDefaultValue('A', 5 * [1,0; -1,4]);
SetDefaultValue('bvec', [5;1]);

x0 = ellipse_parameters.x0;
y0 = ellipse_parameters.y0;
a = ellipse_parameters.a;
b = ellipse_parameters.b;
theta = ellipse_parameters.theta;

%the given ellipse can be written as E * [cos(theta); unit(theta)]
E = [cos(theta), -sin(theta); ...
    sin(theta), cos(theta)] * diag([a b]);

[U,S,V] = svd(A * E);

a_new = S(1);
b_new = S(4);

theta = atan(U(3)/ U(1)); % is this right?

new_centre = A * [x0;y0] + bvec;

new_ellipse_parameters = struct('a', a_new, 'b', b_new, 'theta', theta, 'x0',new_centre(1),'y0',new_centre(2));

if viz
    
    % apply affine transformation to ellipse vector
    ellipse = create_ellipse(ellipse_parameters).';
    ellipse = [real(ellipse); imag(ellipse)];
    ellipse = A * ellipse + + repmat(bvec, [1,128]) ;
    
    clf;
    plot(ellipse(1,:), ellipse(2,:));
    hold on;
    
    % parameterize the ellipse with the new parameters
    plot(create_ellipse(new_ellipse_parameters), 'b')
end