function [err, F, G] = l2errorFMS(p,gammaKm1, gammaObs, L, lambda, inShape, isotropic)

%p - 5x1 vector - (x,y,s,k,theta)  space/scale parameters
%gammaKm1 - nx1 vector -(k-1)st approximation
%gammaObs - nx1 vector - given contour

% z=p(1) + 1i*p(2); %row, col
% s = p(3);
% k = p(4);
% theta = p(5);

if isotropic
  p = [p, 0,0];
end

%% compute optimal gain
gammaKm1zeta = gammaKm1 - (p(1)+1i*p(2));
r = abs(gammaKm1zeta);
adf = exp(p(4) * cos((angle(gammaKm1zeta) - p(5))) - p(4));
rdf = exp(-(r.^2)./(p(3)^2)) .* sin(2*pi*abs(gammaKm1zeta)/p(3));
gk = rdf .* adf .* (gammaKm1zeta./r);

%L = res_old*avLen; %multiplicative constant to go from line integral to area integral

C = 0.2568*pi*p(3)*p(3)*exp(-2*p(4))*besseli(0,2*p(4));

optimalGainNumerator = sum((real(gammaObs-gammaKm1).*real(gk) + imag(gammaObs-gammaKm1).*imag(gk)));
optimalGainDenominator= sum((real(gk).^2 + imag(gk).^2)) + lambda*C/L;

aOpt = optimalGainNumerator./optimalGainDenominator;

if aOpt>.1956*p(3), aOpt=.1956*p(3);
elseif aOpt<-p(3)/(2*pi), aOpt = -p(3)/(2*pi);
end

%% compute error
fGammaKm1 = gammaKm1 + gammaKm1zeta./r .* aOpt .* adf .* rdf;

res = gammaObs-fGammaKm1;
res = sum((real(res).^2 + imag(res).^2));
F = L * res;

G = C * aOpt.^2;

err = F + lambda * G;
%err = L * res + lambda * C * aOpt.^2;


% evaluate penalty numerically
%formlet.z = z; formlet.s = s; formlet.k = k; formlet.theta =theta; formlet.a = aOpt;
%G = Evaluate_ofun_numerically(formlet, gammaObs, fGammaKm1);
%save ('ofun', 'F','G', 'err');

if inShape
    locOutsideContour = ~inpolygon(p(1), p(2), real(gammaKm1), imag(gammaKm1));
else
    locOutsideContour = 0;
end

%if p(3) <0 || p(4)<0
%if p(3) <0 || p(4)<0 || (inShape & ~inpolygon(p(1), p(2), real(gammaKm1), imag(gammaKm1)))
if p(3) <0 || p(4)<0 || locOutsideContour
  err=inf;
end
    

% if vis
%     %plot at each evaluation of the objective function
%     pr= round(p);
%     pr=p;
%     plotter_regions(gammaObs,fGammaKm1, [real(z),imag(z),s,aOpt,k,theta]);
%     title(['F =' num2str(round(F)), ', G = ', num2str(round(G))]);
%     drawnow;    
% end
%%%%%%%%%%%%%%%%%%%%%%%%%


