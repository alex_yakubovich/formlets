function [ellipse, parameters] = fit_pca_ellipse(shape, n)

% ellipse = fit_pca_ellipse(shape) returns an ellipse centred at the mean
% of the shape, and whos major and minor axes are aligned with the eigenvectors
% of the covariance matrix of the shape. Shapes are coded by complex vectors. 
% The general parametric form (x0,y0,a,b, phi) is used, where (x0,y0) is
% the centre, a and b are the lengths of the major and minor axes
% respectively, and phi is the orientation.

% ellipse = fit_pca_ellipse(shape, n) fits an ellipse sampled at n points.

SetDefaultValue('n', 128);

%find principal components
[pc, lambda] = eig(cov([real(shape), imag(shape)]));
[lambda, i] = sort(diag(lambda), 'descend'); %sort eigenvalues in descending order
pc1 = pc(:, i(1)); %largest principal component = major axis

% generate principal components ellipse
theta = atan(pc1(2)/pc1(1)); %orientation determined by major axis
a = sqrt(lambda(1)); %length of major axis
b = sqrt(lambda(2)); %length of minor axis

phi = linspace(0, 2*pi, n)';

ellipse = mean(shape) + ...
    complex(a*cos(phi)*cos(theta) - b*sin(phi)*sin(theta), ...
    a*cos(phi)*sin(theta) + b*sin(phi)*cos(theta));

parameters.x0 = real(mean(shape));
parameters.y0 = imag(mean(shape));
parameters.a = a;
parameters.b = b;
parameters.theta = theta;