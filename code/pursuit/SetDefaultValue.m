function SetDefaultValue(argName, defaultValue)

%if argument doesn't exist or is empty, rela
if ~evalin('caller',sprintf('exist(''%s'',''var'')',argName)) || ...
        evalin('caller',sprintf('isempty(%s)',argName))
    assignin('caller', argName, defaultValue)
end


% if evalin('caller', 'nargin') < position || ...
%       isempty(evalin('caller', argName))
%    assignin('caller', argName, defaultValue);
% end
