function xy = IP(x, y)
%inner product of two complex numbers
  xy = real(x).*real(y) + imag(x).*imag(y);