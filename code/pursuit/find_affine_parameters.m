
function [A,b] = find_affine_parameters(x,y,xobs, yobs)

n = length(x);

X = [sum(x.^2), sum(x.*y), 0, 0, sum(x), 0; ...
    sum(x.*y), sum(y.^2), 0, 0, sum(y), 0; ...
    0, 0, sum(x.^2), sum(x.*y), 0, sum(x); ...
    0, 0, sum(x.*y), sum(y.^2), 0, sum(y); ...
    sum(x), sum(y), 0, 0, n, 0; ...
    0, 0, sum(x), sum(y), 0, n];
        
q = [sum(x.*xobs); sum(xobs .* y); sum(x .* yobs); sum(y.*yobs); sum(xobs); sum(yobs)];

affine_parameters = X\q; %[a,b,c,d, b1,b2]

A = reshape(affine_parameters(1:4), [2 2])';
b = affine_parameters(5:6);