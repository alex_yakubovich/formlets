
function [formletRep1,mapping] = reparameterize2(CM_match, testShape, formletRep, viz)

%CM_match = [testShape, formletRep]
%mapping = [new, old]

SetDefaultValue('viz', 0); %to plot points removed/that are added
n = length(testShape);

viz=1;

if viz
    plotter(testShape, formletRep, [], 1, 0, CM_match);
    ix = cellstr(num2str((1:n)'));
    text(real(testShape), imag(testShape),  ix, 'VerticalAlignment','bottom','HorizontalAlignment','right', 'FontSize', 9)
    text(real(formletRep), imag(formletRep), ix, 'VerticalAlignment','bottom','HorizontalAlignment','right', 'FontSize', 9, 'Color', 'r')
    hold on;
end

t = 1; %index on CM_match
mapping = []; %indices before and afte reparamaterization [new,old]
formletRep1= [];


% bug!! for shape 65, point 100 on model is matched to multiple
% points on target, one of which (point 128) is matched to multiple points
% on the model

%98 100
%99 100
%...
%127 100
%128 100
%128 101
%128 102
%...
%128 106

%% correct matchings on visible portion
while t <= length(CM_match)
    t
    % check how many matches there are on the model    
    p2 = CM_match(t,2);
    reps2 = CM_match(:,2) == p2; 
    %reps2 = CM_match(:,2) == p2 & [diff(CM_match(:,1)) > 0; 1]%
    nreps2 = sum(reps2); %number of matches
    
    if nreps2>1 %interpolate if multiple matches
        
        if isempty(mapping) %t==1
            mapping = [mod((p2:p2+nreps2-1)-1, n)'+1, repmat(p2, nreps2,1)]; %??
        else
            mapping = [mapping; ... %record mapping
                [mod((mapping(end,1)+1:mapping(end,1)+1+nreps2-1)-1, n)'+1, ...
                repmat(mod(mapping(end,2), n)+1, nreps2,1)]];  
        end
        
        %neighbour 1
        if p2>1 % repeating point is not the first on the contour
            if ~isempty(formletRep1)
                prev = formletRep1(end); %last point on the model
            else
                prev = formletRep(p2-1);
            end
        else
            prev = formletRep(end);
        end
        
        %neighbour 2
        if p2<n
            next = formletRep(p2+1);
        else
            next= formletRep(1);
        end
        
        s = linspace(0,1,nreps2+2); %add 2 for flanking points
        s = s(2:(end-1)); %inner points
        
        fnew = (prev * (1-s) + next * s).';
        
        col = 'Cyan';
        t = t+nreps2;
        
        
        
    else     % point on formletRep does not repeat
        %check if point on target repeats
        p1 =  CM_match(reps2,1);
        reps1 = CM_match(:,1) == p1;
        nreps1 = sum(reps1);
        
        if nreps1 > 1 %point on testshape repeats -> take midpoint
            p1f = CM_match(CM_match(:,1)==p1,2); %points on formletRep matched to p1
            fnew = mean(formletRep(p1f));
            
            t = t +  nreps1; % proceed to next point (which hasn't been deleted)
            col = 'Red';
            
            
            if isempty(mapping)  %t==1
                mapping = [mapping; repmat(p2, nreps1,1), p1f];
            else
                mapping = [mapping; ... %record mapping
                    [repmat(mod(mapping(end,1),n)+1, nreps1,1), mod((mapping(end,2)+1:mapping(end,2)+1+nreps1-1)-1, n)'+1]];
            end
            
        else %1:1 matching
            if t==1
                mapping = [mapping; p2, p2];
            else
                mapping = [mapping; mod(mapping(end,:), n)+1];
            end
            
            
            fnew = formletRep(CM_match(t,2));
            t = t+1;
            col = 'Black';
            
            
        end
        
    end
    if viz; plot(fnew, '.', 'Color', col, 'MarkerSize', 10); end
    formletRep1 = [formletRep1; fnew];
end


if viz; plotter(testShape, formletRep1, [], 1, 0); end


