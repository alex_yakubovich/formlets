function [formlet, F,G]= dictionary_descent(testShape,formletRep, pref, dictionary, lambda)

L = find_fidelity_term_constant(testShape, formletRep,pref.n);

selectionScore = dictionary_search(testShape,formletRep, dictionary, lambda, L, pref.inShape)

[formlet, F,G] = selective_descent(testShape, formletRep, selectionScore, dictionary, lambda, L, isotropic, optimizationParameters, inShape)