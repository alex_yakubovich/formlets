function datum = extract_contour(image, visibleSamples, totalSamples, offset, coordinateSystem)
%function datum = extract_contour(image, visibleSamples, totalSamples, offset, rescale)

% datum = Extract_Contour(image, visibleSamples, totalSamples, offset, coordinateSystem)
% loads an image, extracts the contour, and occludes a random contiguous
% portion (n = totalSamples - visibleSamples points). The occluded region
% is determined by offset.


%SetDefaultValue('coordinateSystem', 'original');

datum.image = image;

[datum.boundary, datum.region]  = bwboundaries(image,8,'noholes');
datum.boundary = datum.boundary{1};


%if rescale
if strcmp(coordinateSystem, 'unit-variance-in-each-dimension')
    %zero mean
    datum.boundary = complex(datum.boundary(:,2), -datum.boundary(:,1));
    datum.boundary = datum.boundary-mean(datum.boundary);
    
    %unit variance
    datum.contour = conj(interp1([0; cumsum(abs(diff(datum.boundary)))]/max(cumsum(abs(diff(datum.boundary)))), datum.boundary, (0:totalSamples-1)/(totalSamples))');
    datum.contour = complex(real(datum.contour)/sqrt(sum(real(datum.contour).^2)), imag(datum.contour)/sqrt(sum(imag(datum.contour).^2)));
    
    datum.target.entire = circshift(datum.contour, offset);   %random shift
   % datum.target.entire = 100 * datum.target.entire ;
     datum.target.entire = datum.target.entire - mean(datum.target.entire(1:visibleSamples)); %(redundant)
    
    
% elseif strcmp(coordinateSystem, 'unit-max-dimension')
%     
%     datum.boundary = complex(datum.boundary(:,1), datum.boundary(:,2)); %r+ci
%     datum.contour = conj(interp1([0; cumsum(abs(diff(datum.boundary)))]/max(cumsum(abs(diff(datum.boundary)))), datum.boundary, (0:totalSamples-1)/(totalSamples))');    
%         
%     %distance matrix
%     x = real(datum.contour);
%     y = imag(datum.contour);    
%     bR= repmat([x, y],1,totalSamples);
%     MR=reshape([x'; y'], 1, 2*totalSamples); MR = repmat(MR, totalSamples, 1);
%     dt=(MR-bR).^2; D1= sqrt(dt(:,1:2:end-1) + dt(:, 2:2:end) ); %  element(r,c) is the distance of rth point on a from cth point on b
%     dt = abs(MR-bR);
%     Dx = dt(:,1:2:end-1); %Dx[i,j] = |xi - xj |
%     Dy = dt(:, 2:2:end); %Dy[i,j] = =|yi - yj| 
%     
%     datum.contour = datum.contour/max(Dx(:)), max(Dy(:)); %normalize shape so that max(height,width) =1
%     
%     datum.target.entire = circshift(datum.contour, offset);   
%     
%     [x,y] = poly2ccw(imag(datum.target.entire), -real(datum.target.entire));
%      datum.target.entire = complex(x, y);
%     
    
     
     
%elseif strcmp(coordinateSystem, 'original') | strcmp(coordinateSystem, 'unit-max-dimension')
else
    datum.boundary = complex(datum.boundary(:,1), datum.boundary(:,2)); %r+ci
    datum.contour = conj(interp1([0; cumsum(abs(diff(datum.boundary)))]/max(cumsum(abs(diff(datum.boundary)))), datum.boundary, (0:totalSamples-1)/(totalSamples))');
       
    datum.target.entire = circshift(datum.contour, offset);   
    
    [x,y] = poly2ccw(imag(datum.target.entire), -real(datum.target.entire));
    %[x,y] = poly2cw(imag(datum.target.entire), -real(datum.target.entire)); %orient clockwise
     datum.target.entire = complex(x, y);
end

%compute final target shape, both occluded and visible portion
datum.target.visible = datum.target.entire(1:visibleSamples);
datum.target.occluded = datum.target.entire(visibleSamples+1 : end);

