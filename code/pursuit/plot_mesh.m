function plotMesh(m)
for col = 1:size(m,2)
     hold on;
    %plot(m(:,col), 'k-', 'MarkerSize', 3, 'color', .6*[1 1 1]);    
    %plot(m(:,col), 'LineStyle',  '-', 'color', .8*[1 1 1]);
    plot(m(:,col), 'k-', 'LineWidth', 1, 'color', .7*[1 1 1]);    
end
for row = 1:size(m,1)
    %plot(m(row,:), 'k-', 'MarkerSize', 3, 'color', .6*[1 1 1]);
    %plot(m(row, :), 'LineStyle',  '-', 'color', .8*[1 1 1]);
    plot(m(row,:), 'k-', 'LineWidth', 1, 'color', .7*[1 1 1]);
end