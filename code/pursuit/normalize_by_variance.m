function [shapeletRep, datum, var2d] = normalize_by_variance(shapeletRep, datum)

var2d = var(abs(datum.target.visible - mean(datum.target.visible)));

shapeletRep.entire = shapeletRep.entire/var2d;
shapeletRep.visible = shapeletRep.visible/var2d;
shapeletRep.occluded= shapeletRep.occluded/var2d;

datum.contour = datum.contour/var2d; %normalize shape so that max(height,width) =1
datum.target.entire = datum.target.entire/var2d;
datum.target.visible= datum.target.visible/var2d;
datum.target.occluded = datum.target.occluded/var2d;