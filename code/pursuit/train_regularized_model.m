
load('../../data/preferences.mat')
%pref = define_preferences(0.1, 0);

%nfiles = length(pref.dataset.filelist)
pref.dataset.train_files = randi(nfiles, 128,1);
pref.dataset.filelist = pref.dataset.filelist(pref.dataset.train_files )
pref.nfiles = length(pref.dataset.filelist);


lambda_grid = [0, linspace(0.0002, 0.0006, 16)];
%lambda_grid = lambda_grid(2:end);
for lambda = lambda_grid
    pref.fileName = ['train-64-formlets-lambda-' num2str(lambda) '.mat']
    pref.lambda = lambda;
    formlet_data = formlet_pursuit(pref);
end
