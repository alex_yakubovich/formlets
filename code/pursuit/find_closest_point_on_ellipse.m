
function [closest_phi, closest_point] = find_closest_point_on_ellipse(query_point,ellipse_parameters)

%INPUT
%xp,yp - coordinates of query point
%x0,y0,a,b,theta- ellipse parameters

SetDefaultValue('viz', false);

if nargin > 0
    x0 = ellipse_parameters.x0;
    y0 = ellipse_parameters.y0;
    a = ellipse_parameters.a;
    b = ellipse_parameters.b;
    theta = ellipse_parameters.theta;    
    
    xp = real(query_point);
    yp = imag(query_point);
else
    x0 = -1;
    y0 = 5;
    a = 5;
    b = 10;
    theta = 3*pi/4;
end

%1. map query point onto ellipse coordinates
xp_e = (xp-x0)*cos(theta) + (yp-y0)*sin(theta); %ellipse coordinates
yp_e = -(xp-x0)*sin(theta) + (yp-y0)*cos(theta);

%2. find closest point on ellipse (ellipse-centric coordinates)
%%%%%%%%%%%%%%%

c2 = a^2 - b^2;

%find roots of fourth degree polynomial in t
t_roots = roots([yp_e*b, 2*xp_e*a+2*c2,0,2*xp_e*a - 2*c2,-b*yp_e]); 

t_roots = t_roots(imag(t_roots)==0); %real roots

phi_roots = 2*atan(t_roots); 

%3. map back to coordinate system centred at origin

ellipse_roots = complex(x0 + a*cos(phi_roots)*cos(theta) - b*sin(phi_roots)*sin(theta), ...
    y0 + a*cos(phi_roots)*sin(theta) + b*sin(phi_roots)*cos(theta));

[~, i] = min(abs(ellipse_roots - complex(xp,yp))); 

closest_phi = mod(phi_roots(i), 2*pi);

closest_point = ellipse_roots(i);

if viz
    
    n = 128;
    
    phi = linspace(0, 2*pi, n)';

    ellipse = complex(x0 + a*cos(phi)*cos(theta) - b*sin(phi)*sin(theta), ...
    y0 + a*cos(phi)*sin(theta) + b*sin(phi)*cos(theta));

    clf;
    plot(ellipse);
        
    hold on; plot(xp,yp,'x');
    
    plot(closest_point, 'b.')
    %plot(ellipse_roots, 'b.')
    
    axis image;
end