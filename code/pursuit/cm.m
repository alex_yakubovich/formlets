
function [trace, rot] = cm(gammaA, gammaB, searchRotations, viz)

SetDefaultValue('viz', 0);
SetDefaultValue('searchRotations', 0);
 ia = real(gammaA); %visible curve
 ja = imag(gammaA);
 
 ib = real(gammaB);%model
 jb = imag(gammaB);
 
 
%ensure both curves are oriented clockwise
%  flgCWA=ispolycw(ja,ia); flgCWB=ispolycw(jb,ib);
%  [ja, ia]= poly2cw(ja, ia); [jb, ib]= poly2cw(jb, ib);
 
%  recursive
 big = 1e+10;
 n1=length(ia); 
 n2=length(ib); 
 
 % find Euclidean distance between all pairs of (visible) points (n1xn2 table)
 bR= repmat([ia, ja],1,n2);
 MR=reshape([ib'; jb'], 1, 2*n2); MR = repmat(MR, n1, 1);
 dt=(MR-bR).^2; D1= sqrt(dt(:,1:2:end-1) + dt(:, 2:2:end) ); %  element(r,c) is the distance of rth point on a from cth point on b
 
% cost of matching vis to occluded points = infinity
% cost of matching vis to occluded points = 0
% pad table with zeros for occluded points

Dfull = zeros(n1, n2); 
Dfull(1:n1, 1:n2) = D1;
%Dfull(1:n1, n2-occludedSamples+1:n2) = inf; %uncomment to restrict some points on the model from being matched to occluded target points

D1 = Dfull;

%Dfull = inf(n1 + occludedSamples);
%Dfull = zeros(n1 + occludedSamples);
%Dfull(1:n1, 1:n2) = D1;
%Dfull(n1+1:end, n1+1:end) = 0;
%D1 = Dfull;

%n1 = n1 + occludedSamples;
%n2 = n2 + occludedSamples;
%n2=n1;
 bestcost=big;
 %C=zeros(n1,n1);
 C=zeros(n1,n2); % the dynamic programming table
 YI=zeros(size(C)); %  saving the moves on the DP table to make backtracking easy
 % YI=1  :coming from neighbor on left(W),
 % YI=2 :coming from neighbor on top (N),
 % YI=3 :coming from neighbor on top left (NW)

 if searchRotations
     rotations = 1:n2;
 else
     rotations = 1;
 end
 for rot = rotations%1:1%n2

    % rotate distance table accordingly
    %D=[D1(:,rot:n2), D1(:,1:rot-1)];
    D=[D1(1:n1,rot:n1), D1(1:n1,1:rot-1)];    
    C=0 * C; YI= 0 * YI; %reset to zero

    % DP
    C(1,1)=D(1,1);
    for j=2:size(C,2), C(1,j)= C(1, j-1) + D(1,j); YI(1,j)=1; end; %  first row
    for i=2:size(C,1), C(i,1)= C(i-1, 1) + D(i,1); YI(i,1)=2; end; %  first column
    for i=2:size(C,1)
        for j=2:size(C,2)
            y=C(i,j-1); yi=1;
            if C(i-1,j)<y, y= C(i-1,j); yi=2; end;
            if C(i-1,j-1)<y, y=C(i-1, j-1); yi=3; end;
            %[y, yi]= min([C(i,j-1), C(i-1,j), C(i-1,j-1)]); %above way  isfaster
            C(i,j)= D(i,j) + y; YI(i,j)= yi;
        end;
    end

    % keep info if this is the best so far
    if C(n1, n2)<=bestcost
        bestrot=rot;
        YIbest=YI;
        bestcost=C(n1, n2);
    end;
 end;

 % now we have the best rotation and its YI to backtrack
 rot=bestrot;
 trace=zeros(n1+n2,2); %maximum number of points on trace is n1+n2
 YI=YIbest;

 % backtrack on DP table
 j=n2; ct=1; i=n1;
 trace(ct,:)=[i j];
 while ~((i==1) && (j==1))
    ct=ct+1;
    switch YI(i,j)
        case 1, j=j-1; trace(ct,:)=[i j];
        case 2, i=i-1; trace(ct,:)=[i j];
        case 3, i=i-1; j=j-1; trace(ct,:)=[i j];
    end;
 end;
 trace(ct+1:end,:)=[];
 
 trace(:,2)=  mod(trace(:,2) +rot -2, n2) +1;  %rotation
  
 if viz
     figure;
     D=[D1(:,bestrot:n2), D1(:,1:bestrot-1)];
     D(sub2ind(size(D), trace(:,1), trace(:,2))) = max(D(:));
     imagesc(D);
     xlabel 'model';
     ylabel 'target';
     title(sprintf('matching cost %i', bestcost))
     %saveas(gcf, 'CM distance matrix showing optimal path, unconstrained', 'png')
 end
 
 
 %CM= bestcost /ct;

%  if ~flgCWA, [ja, ia]= poly2ccw(ja, ia); trace(:,1)=length(ia)+1-trace(:,1);
% end
%  if ~flgCWB, [jb, ib]= poly2ccw(jb, ib); trace(:,2)=length(ib)+1-trace(:,2);end

 
 %force a 1-1 match on occluded region
%  if occludedSamples>0
%      trace = trace(trace(:,1) <= n1-occludedSamples, :); %ignore CM match on visible portion
%      trace = [repmat((length(ia)+occludedSamples:-1:length(ia))', [1,2]); trace];
%  end
 
 trace = flipud(trace); %%ensure matching preserves original ordering

 % [~, ix] = sort(trace); 
% trace = trace(ix(:,1), :);
  