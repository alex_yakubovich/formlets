function ellipse = find_occluded_region(ellipse_parameters, shape, visibleSamples, occludedSamples)
% given ellipse parameters [x0,y0,a,b,theta] and and the visible part of
% the target contour, divide the ellipse into the visible and occluded
% regions

[phi1, p1] = find_closest_point_on_ellipse(shape(1), ellipse_parameters);
[phi2, p2] = find_closest_point_on_ellipse(shape(end), ellipse_parameters);

%[phi3, p3] = find_closest_point_on_ellipse(shape(2), ellipse_parameters);
%plot(p1, '.k')
%plot(p2, '.k')
%plot(p3, '.m')
phi_start = min(phi1, phi2);
phi_end = max(phi1, phi2);


%partition [0,2*pi] into two intervals, I1 and I2
phi_interval1 = phi_end - phi_start;
phi_interval2 = (2*pi - phi_end) + phi_start;       

%option 1 - map I1 to occluded part, I2 to visible part
phi_occluded1 = linspace(phi_start, phi_end, occludedSamples+2);
phi_visible1 = [linspace(phi_end, 2*pi, round(visibleSamples * (2*pi - phi_end)/phi_interval2)), ...
    linspace(0, phi_start, round(visibleSamples * phi_start/phi_interval2))];

%option 1 - map I1 to visible part, I2 to occluded part
phi_visible2 = linspace(phi_start, phi_end, round(visibleSamples));
phi_occluded2 = [linspace(phi_end, 2*pi, round((occludedSamples+2) * (2*pi - phi_end)/phi_interval2)), ...
    linspace(0, phi_start, round((occludedSamples+2) * phi_start/phi_interval2))];

%e1 = create_ellipse(ellipse_parameters, phi_visible1) - shape;
%e1 = sum(real(e1).^2 + imag(e1).^2)

%e2 = create_ellipse(ellipse_parameters, phi_visible2) - shape;
%e2 = sum(real(e2).^2 + imag(e2).^2)

ellipse1 = create_ellipse(ellipse_parameters, phi_visible1);
e1 = l2nonparam([real(shape), imag(shape)], [real(ellipse1), imag(ellipse1)]);

ellipse2 = create_ellipse(ellipse_parameters, phi_visible2);
e2 = l2nonparam([real(shape), imag(shape)], [real(ellipse2), imag(ellipse2)]);


if e1 < e2
    phi_visible = phi_visible1;
    phi_occluded = phi_occluded1;        
else
    phi_visible = phi_visible2;
    phi_occluded = phi_occluded2;    
end
 phi_occluded = phi_occluded(2:end-1);

% d = abs(mod(phi3 - [phi_start,phi_end],2*pi)); %angular distance b/w second visible point and two endpoints
% 
% if d(1) > d(2) %phi_interval1 < phi_interval2
%     phi_occluded = linspace(phi_start, phi_end, occludedSamples+2);          
%     phi_visible = [linspace(phi_end, 2*pi, round(visibleSamples * (2*pi - phi_end)/phi_interval2)), ...
%         linspace(0, phi_start, round(visibleSamples * phi_start/phi_interval2))];
% else        
%     phi_visible = linspace(phi_start, phi_end, round(visibleSamples));
%     phi_occluded = [linspace(phi_end, 2*pi, round((occludedSamples+2) * (2*pi - phi_end)/phi_interval2)), ...
%         linspace(0, phi_start, round((occludedSamples+2) * phi_start/phi_interval2))];
% end
% phi_occluded = phi_occluded(2:end-1);

%figure; plotter(shape, create_ellipse(ellipse_parameters, phi_visible1), [], 1);
%title(sprintf('l2 nonparametric error = %0.2f', e1))
%saveas(gcf, 'giraffe - parameterization 1.eps', 'epsc')
%figure; plotter(shape, create_ellipse(ellipse_parameters, phi_visible2), [], 1);
%title(sprintf('l2 nonparametric error = %0.2f', e2))
%saveas(gcf, 'giraffe - parameterization 2.eps', 'epsc')

phi_visible_rev = phi_visible(end:-1:1);
phi_occluded_rev = phi_occluded(end:-1:1);

e1 = create_ellipse(ellipse_parameters, phi_visible) - shape;
e1 = sum(real(e1).^2 + imag(e1).^2);

e2 = create_ellipse(ellipse_parameters, phi_visible_rev) - shape;
e2 = sum(real(e2).^2 + imag(e2).^2);

if e1<e2
    ellipse.visible = create_ellipse(ellipse_parameters, phi_visible);
    ellipse.occluded = create_ellipse(ellipse_parameters, phi_occluded);
else
    ellipse.visible = create_ellipse(ellipse_parameters, phi_visible_rev);
    ellipse.occluded = create_ellipse(ellipse_parameters, phi_occluded_rev);
end

% ellipse.visible = create_ellipse(ellipse_parameters, phi_visible);
% ellipse.occluded = create_ellipse(ellipse_parameters, phi_occluded);
 ellipse.entire= [ellipse.visible; ellipse.occluded];
