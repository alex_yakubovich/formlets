

load('../../data/preferences.mat')


scalingFactor = zeros(1,pref.nfiles);

for file = 1:pref.nfiles
    
    fileName = pref.dataset.filelist(file).name; %get filename
    image = imread([pref.dataset.directory fileName]);
    
    oldDatum = extract_contour(image, pref.visibleSamples, pref.n, pref.offset(file), 'unit-variance-in-each-dimension');
    
    newDatum = extract_contour(image, pref.visibleSamples, pref.n, pref.offset(file), 'original');
    [newDatum, ~] = normalize_shapes([], newDatum, 'unit-variance');
    
    oldShape = oldDatum.target.entire;
    newShape = newDatum.target.entire;
    
    scalingFactor(file) = std(newShape)/std(oldShape);
    
    %oldShape1 = oldShape * std(newShape)/std(oldShape);
    %newShape1 = newShape * std(oldShape)/std(newShape)        
        
end

mean(scalingFactor)

