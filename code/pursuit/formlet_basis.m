function [ Gammap ] = formlet_basis(Gamma, formlet)
%FORMLET_BASIS Formlet basis function.

%Gammap = Gamma + a*(((Gamma-z)./abs(Gamma-z)) .* sin((2*pi*abs(Gamma-z))/s) .* exp(-(abs(Gamma-z).^2)/(s^2)));
%A = circ_vmpdf(angle(Gamma-z), theta, k)/circ_vmpdf(theta,theta,k);  %Angular Deformation Fn: Von-mises pdf, normalized to have a mode of 1
%R = abs(Gamma-z) .* exp(-(abs(Gamma-z).^2)/(s^2)); %Radial Deformation Function

if ~isfield(formlet, 'k')    
    formlet.k=0; formlet.theta=0; %isotropic formlet
end

if ~isstruct(Gamma)
    Gammap = apply_formlet(Gamma, formlet.z, formlet.s, formlet.a, formlet.k, formlet.theta);
else
    Gammap.entire = apply_formlet(Gamma.entire, formlet.z, formlet.s, formlet.a,formlet.k,formlet.theta);
    
    nvisible = length(Gamma.visible);
    n = length(Gamma.entire);
    
    Gammap.visible = Gammap.entire(1:nvisible);
    Gammap.occluded = Gammap.entire(nvisible+1:n);
end

end

function Gammap = apply_formlet(Gamma, z, s, a,k,theta)
R = exp(-(abs(Gamma-z).^2)/(s^2)) .* sin(2*pi*(abs(Gamma-z))/s); %Radial Deformation Function
A = exp(k * cos(angle(Gamma-z) - theta))/exp(k); %anisotropic formlets
Gammap = Gamma + ((Gamma-z)./abs(Gamma-z)) .* a .* A .* R;
end
