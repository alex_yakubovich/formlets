function pref = define_preferences(occlusion, lambda, offset)
% Define preferences for synthesizing the occluded contours and for the
% shapelet and formlet methods. 

%SetDefaultValue('lambda', 0.003942);
SetDefaultValue('lambda', 0);
SetDefaultValue('occlusion', 0);
    
pref.occlusion = occlusion;
pref.lambda = lambda;

pref.n = 128; %contour resolution
%pref.occlusion  = 0;
pref.niterations = 32; 

%shapelet dictionary
pref.sigma = 0;          %shapelet attenuation or basis selector
pref.bound = 1;          %upper bound of shapelet scale parameter

%formlet dictionary
% (N*d)^2 locations from -w to w
% N*d/4 locations from 0 to 2w

%pref.d = 0.8;
pref.d = 0.4;                  %formlet density
pref.w = 0.4;                  %formlet span

%formlet gradient descent
pref.optimizationParameters.nSeeds = 25;
pref.optimizationParameters.fTol = 10^-6;
pref.optimizationParameters.xTol = 10^-4;

pref.inShape = 0;

% generate occluded contours based on preferences
pref.occludedSamples = round(pref.occlusion*pref.n); % # of occluded samples
pref.visibleSamples = pref.n-pref.occludedSamples; % # of visible samples

%pref.imageType = 'rescaled';
pref.dataset.directory = '~/Dropbox/Thesis/252 Animal/';
%pref.dataset.directory = '~/Dropbox/Thesis/391 Animal/';
%pref.dataset.directory = '~/Dropbox/Thesis/391 Animal/Rescaled/';
pref.dataset.filelist = dir(fullfile(pref.dataset.directory, '*.gif'));
pref.nfiles = length(pref.dataset.filelist);


%files to run
%pref.n1 = 1;
%pref.n2 = 391;

%pref.init = 'circle';
%pref.init = 'ellipse';
%pref.init = 'ellipse-numerical';
pref.init = 'ellipse-analytical-optimal';

pref.coordinateSystem = 'original';
%pref.coordinateSystem = 'unit-variance';
%pref.coordinateSystem = 'unit-variance-in-each-dimension';
%pref.coordinateSystem = 'unit-max-dimension';


%pref.optPair = 'none';
%pref.optPair = 'cyclic-shifts';
pref.optPair = 'CM';

%pref.algorithm  = 'heuristic_descent';
pref.algorithm = 'dictionary_descent';
 
pref.weights= 'uniform';

%index training images
% pref.nfiles = 64;
% pref.seed = rng;
% pref.dataset.fileNums = ceil(rand(pref.nfiles,1)*length(pref.dataset.filelist));
% pref.dataset.filelist = pref.dataset.filelist(pref.dataset.fileNums);

%pref.dataset.filelist = pref.dataset.filelist(1:128); %only use first 128 images for training
%pref.dataset.filelist = pref.dataset.filelist(1:64); %only use first 64 images for training

if pref.nfiles==0
  error('Dataset directory specified does not contain any *.gif files! Check that you have the right directory')
end
  
  %pref.offset = round(unifrnd(0, pref.n)); % randomize occluded region (same occluded region for every image)
%  pref.offset = round(unifrnd(0, pref.n)) * ones(1, pref.nfiles); % randomize occluded region (same occluded region for every image)
 
 %load('../../data/seed.mat');
 %rng(seed);
 %pref.offset = round(unifrnd(0, pref.n, pref.nfiles, 1)); % different occluded region for every image
 SetDefaultValue('offset', round(unifrnd(0, pref.n, pref.nfiles, 1)));
 pref.offset = offset; 
 
 
  %occluded region used in JIVC paper
%   if pref.occlusion==0
%     pref.offset=0;
%   elseif pref.occlusion==0.1
%     pref.offset = 121;
%   elseif pref.occlusion==0.3
%     pref.offset = 126;
%   else
%     pref.offset = round(unifrnd(0, pref.n));
%   end
        
  pref.randomSeed = rng; %save seed

%plotting preferences
pref.plot.iter = 1; %to plot each formlet iteration
pref.plot.res = 1; %to show each residual
pref.plot.grid = 1;
pref.plot.solver = 0; %to show each step of the solver
pref.plot.save = 0; %save figures

pref.save = 1; %save data
pref.niterations=32;

pref.isotropic = 0 ; %set to 1 to get oriented formlets

