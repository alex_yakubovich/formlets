function m = plotter(testShape, formletRep, formlet, showRes, showGrid, ix, weights, formletColor, m, lwidth, plotBoth)
%function m = plotter(testShape, formletRep, formlet, showRes, showGrid, ix, weights, formletColor, m, lwidth, plotBoth)


SetDefaultValue('plotBoth', 1);
SetDefaultValue('showRes', 0);
SetDefaultValue('showGrid', 1);
SetDefaultValue('formlet', struct('z', 0, 's', 1, 'a', 0, 'k', 0, 'theta', 0));
SetDefaultValue('weights', []);
SetDefaultValue('ix', []);
SetDefaultValue('formletColor', [0 .6 0]);
SetDefaultValue('m', []);
SetDefaultValue('lwidth', 2);

cla;
%clf;

if ~isstruct(testShape)
    
    testShape1 = testShape;
    clear testShape;
    
    testShape.visible = testShape1;
    testShape.entire= testShape.visible;
    testShape.occluded= [];
    
    formletRep1 = formletRep;
    clear formletRep;
    formletRep.visible = formletRep1;
    formletRep.entire= formletRep.visible;
    formletRep.occluded= [];
end


%% plot grid
if showGrid
    if ~isstruct(formlet) %if formlet is a vector instead of a structure
        p=formlet;
        clear formlet;
        formlet.z = p(1)+1i*p(2);
        formlet.s = p(3);
        formlet.a = p(4);
        formlet.k = p(5);
        formlet.theta = p(6);
    end
    
    if isempty(m) %build grid
        m = build_mesh(testShape.visible, formletRep.visible);
    end
    m = formlet_basis(m, formlet);
    plot_mesh(m);
end

%% plot shapes

if ~isstruct(testShape)
    testShape.visible = testShape;
    testShape.entire= testShape.visible;
    testShape.occluded= [];
    
    formletRep.visible = formletRep;
    formletRep.entire= formletRep.visible;
    formletRep.occluded= [];
end

if isempty(testShape.occluded) | length(testShape.occluded)==0
    
    if plotBoth
        plot([testShape.visible(end); testShape.visible], 'k', 'LineWidth', lwidth); hold on;
    end
    
    plot([formletRep.visible(end); formletRep.visible], 'Color', formletColor, 'LineWidth', lwidth);
    %     plot(testShape.visible, 'k', 'LineWidth', 2); hold on;
    %     plot(formletRep.visible, 'Color', formletColor, 'LineWidth', 2);
else
    
    if plotBoth
        plot([testShape.occluded(end); testShape.visible; testShape.occluded(1)], 'k', 'LineWidth', 2); hold on;
    end
    plot([formletRep.occluded(end); formletRep.visible; formletRep.occluded(1)], 'Color', formletColor, 'LineWidth', 2);
end

%plot(testShape.occluded, 'r--', 'LineWidth', 2); hold on;
%plot(formletRep.occluded, 'r--', 'LineWidth', 2);

if plotBoth
    plot(testShape.occluded, 'k--', 'LineWidth', lwidth); hold on;
end
plot(formletRep.occluded, '--', 'Color', formletColor, 'LineWidth', lwidth);

%plot(testShape.occluded, 'k', 'LineWidth', 2); hold on;
%plot(formletRep.occluded, '-', 'Color', formletColor, 'LineWidth', 2);

hold on;
axis image;
axis off;

%% plot residual

if showRes
    C=colormap;
    caxis([0 1])
    ncolors = length(C);
    
    % use color to display the weights
    
    if isempty(ix)
        
        if isempty(weights)
            Hvis = line([real(testShape.visible).'; real(formletRep.visible).'],...
                [ imag(testShape.visible).'; imag(formletRep.visible).'], 'LineWidth', 1, 'Marker', '.');
        else
            Hvis = line([real(testShape.visible(weights==1)).'; real(formletRep.visible(weights==1)).'],...
                [ imag(testShape.visible(weights==1)).'; imag(formletRep.visible(weights==1)).'], 'LineWidth', 1, 'Marker', '.');
        end
        
        %plot(testShape.occluded, 'r.');
        %plot(formletRep.occluded, '.r');
        %Hocc = line([real(testShape.occluded).'; real(formletRep.occluded).'],...
        %    [ imag(testShape.occluded).'; imag(formletRep.occluded).'], 'LineWidth', 1, 'Marker', '.');
        
    else
        
        H = line([real(testShape.entire(ix(:,1))).'; real(formletRep.entire(ix(:,2))).'],...
            [ imag(testShape.entire(ix(:,1))).'; imag(formletRep.entire(ix(:,2))).'], 'LineWidth', 1, 'Marker', '.');
        
        visibleSamples = length(formletRep.visible);
        visRows= (ix(:,1) <= visibleSamples); %& (ix(:,2) <= visibleSamples);
        Hvis = H(visRows);
        % Hocc = H(~visRows);
    end
    
    %set(Hvis, 'LineStyle', '-', 'Color', [0 .6 0]);
    %set(Hvis, 'LineStyle', '-', 'Color', [0 0 1]);
    %set(Hocc, 'LineStyle', '-', 'Color', [1 .3 .3]);
    
    set(Hvis, 'LineStyle', '-', 'Color', [0 .6 0]);
    %set(Hocc, 'LineStyle', '-', 'Color', [.9 0 0]);
    
    %   set(Hvis, 'LineStyle', '-', 'Color', [0.3 0.3 0.3]);
    %   set(Hocc, 'LineStyle', '-', 'Color', [0 .6 0]);
    
    %       set(Hvis, 'LineStyle', '-', 'Color', [0 0 .6]);
    %       set(Hocc, 'LineStyle', '-', 'Color', [1 .3 .3]);
end

% if ~noFormletFlag
%     plot(real(formlet.z), imag(formlet.z), 'x', 'MarkerSize', 12, 'Color', formletColor);
% end

drawnow;

%% print objective functions

% vis_res = testShape.visible - formletRep.visible;
% vis_res = sqrt(sum((real(vis_res).^2 + imag(vis_res).^2)));
%
% if isempty(testShape.occluded) | length(testShape.occluded)==0
%     s = sprintf('vis. err = %d',  round(vis_res));
% else
%     occ_res = testShape.occluded - formletRep.occluded;
%     occ_res = sqrt(sum((real(occ_res).^2 + imag(occ_res).^2)));
%     s = sprintf('vis. err = %d, occ. err = %d',  round(vis_res), round(occ_res));
% end
% title(s);




