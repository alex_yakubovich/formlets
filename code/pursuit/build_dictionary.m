
function formlets = build_dictionary(pref,target)

%Build formlet dictionary, with round(N*d) location parameters linearly sampled
% between -w and w and round(N*d/4) scale parameters between 0 and 2w. (52x52x13 disctionary if density=.4)


if strcmp(pref.coordinateSystem, 'original')
  % work in original image coordinates
  
  %if strcmp(pref.imageType, 'rescaled')
  %imdim = [min(imag(datum.target.entire)), max(imag(datum.target.entire)), -min(real(datum.target.entire)), -max(real(datum.target.entire))];
  imdim = [min(real(target.visible)), max(real(target.visible)), min(imag(target.visible)), max(imag(target.visible))];
  %else
  %    imdim = [0, size(datum.image,2), -size(datum.image,1),0];
  %end
  
  %keyboard
  
  %nx =  round(pref.n * pref.d/4 * range(imdim(1:2))/range(imdim(3:4)));
  %ny =  round(pref.n * pref.d/4 * range(imdim(3:4))/range(imdim(1:2)));
  
  nx = round(pref.n * pref.d/4);
  ny = nx;
  %
  formlets.xspaces = linspace(imdim(1), imdim(2), nx); %columns
  formlets.yspaces = linspace(imdim(3), imdim(4), ny); %rows
  
  %for grid of constant density
  %formlets.xspaces = imdim(1) : pref.n * pref.d : imdim(2);
  %formlets.yspaces = imdim(3) : pref.n * pref.d : imdim(4);
  
  pref.w = max(imdim(:))/2;
  
  [i1, i2] = ndgrid(formlets.xspaces, formlets.yspaces); %(x,y) grid
  
else
  %work in normalized coordinate system
  formlets.xspaces = linspace(-pref.w, pref.w, round(pref.n * pref.d)); %xy
  formlets.yspaces = formlets.xspaces;
  [i1, i2] = ndgrid(formlets.xspaces, formlets.yspaces);  
end

%formlets.Z_Map = repmat(complex(i1, i2), [1,1,pref.n-round(pref.occlusion*pref.n)]);
formlets.Z_Map = complex(i1, i2);

if pref.isotropic
  formlets.k = 0;
  formlets.theta0=0;
  formlets.scales = linspace(2*pref.w/round(pref.n* pref.d/4), 2*pref.w, round(pref.n*pref.d/4));
  
else
  formlets.k = linspace(0,200,round(pref.n*pref.d/4));
  formlets.theta0 = linspace(-pi,pi,round(pref.n*pref.d/4));
  formlets.scales = linspace(2*pref.w/round(pref.n* pref.d/4), 2*pref.w, round(pref.n*pref.d/4));
  
  %formlets.scales = linspace(2*pref.w/round(pref.n* pref.d/4), 2*2500, round(pref.n*pref.d/4));
  % this dictionary does not work well in normalized coordinates!!
  % coordinates!
end

%% plot locations and scales
 %hold on;
 %plot(formlets.Z_Map(:), '.', 'Color', [.4 .4 .4]);
 %plot(formlets.Z_Map(:,:,1).', 'Color', [.4 .4 .4]);

%  p0=1.2*1i*imdim(4);
%  for s = formlets.scales
%      circle = p0 + s*exp(1i*(0:.01:2*pi));
%      p0 = p0 + 200;
%      plot(circle)
%      axis tight
%  end
