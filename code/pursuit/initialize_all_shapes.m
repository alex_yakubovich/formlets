function init = initialize_all_shapes(pref, viz)

SetDefaultValue('viz', 0);

init = cell(1,pref.nfiles);

%problematic_files = [21, 25, 71, 91, 185, 187, 200];
%for file = problematic_files %1:pref.nfiles
for file = 1:pref.nfiles
    tic
    fileName = pref.dataset.filelist(file).name;
    image = imread([pref.dataset.directory fileName]);
            
    datum = extract_contour(image, pref.visibleSamples, pref.n, pref.offset(file), pref.coordinateSystem);
    datum.name = fileName;        
    
    init{file} = initialize_pursuit(datum.target, datum.region, pref.visibleSamples, pref.n, pref.init, pref.optPair);        
    
    
    subplot(2,1,1);
    imagesc(image);
    axis image;
    
    subplot(2,1,2);
    plot(datum.target.entire);
    hold on;
    plot(datum.target.entire(1), '.k');
    axis image;
    clf;
    
    if viz
        plotter(datum.target, init{file}, [],1);        
        saveas(gcf, ['init-file-', num2str(file),'-fix-occluded-samples'], 'epsc');        
        %saveas(gcf, ['init-file-', num2str(file),'-fix-occluded-samples-1-iteration'], 'epsc');        
    end
    
    fprintf('computed ellipse initialization for shape %i/%i in %0.1f seconds \n', file, pref.nfiles, toc)
end