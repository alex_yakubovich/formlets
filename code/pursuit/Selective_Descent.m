function [formlet,F,G] = Selective_Descent(testShape, formletRep, selectionScore, dictionary, lambda, L, isotropic, optimizationParameters, inShape)

% Selective_Descent(testShape, formletRep, selectionScore, dictionary, lambda, L, isotropic, nSeeds,fTol,xTol, inShape)
% runs a numerical optimization (fminsearch for now), initialized from nSeeds points. The initializations used are the 
% the points in dictionary with the lowest error values in selectionScore. 

fTol = optimizationParameters.fTol;
xTol = optimizationParameters.xTol;
nSeeds = optimizationParameters.nSeeds;

errorSort = unique(selectionScore(:));  %sort error in ascending order, only keeping unique values

options = optimset('Display', 'off', 'Jacobian','off', 'DerivativeCheck', 'off', 'FinDiffType', 'Central', 'Diagnostics', 'off', 'TolFun', fTol, 'TolX', xTol);

if isotropic
  gradDescMin = zeros(nSeeds, 4);
else
  gradDescMin = zeros(nSeeds, 6);
end

plotEverySeed = 0;

for seed=1:nSeeds

  [i,j,k,l,m] = ind2sub(size(selectionScore), find(selectionScore==errorSort(seed))); %index of seed
  i=i(1);j=j(1);k=k(1);l=l(1);m=m(1);    
    
    p0 = [real(dictionary.Z_Map(i,j,1)), imag(dictionary.Z_Map(i,j,1)), dictionary.scales(k)];
    if ~isotropic
      p0 = [p0, dictionary.k(l), dictionary.theta0(m)];
    end   
    
    if plotEverySeed
        apply_formlet_and_plot(testShape, formletRep, [x0,y0,s0,k0,theta0], 1, L);        
    end
    
    [gradDescMin(seed,1:end-1), gradDescMin(seed,end)] = fminsearch(@(p)l2errorFMS(p,formletRep, testShape, L, lambda, inShape, isotropic), p0, options); %deploy pursuit starting at seed
    
     if plotEverySeed
         apply_formlet_and_plot(testShape, formletRep, gradDescMin(seed,1:5), 1, L);               
     end
   
end

%fminsearch
pOptimal = gradDescMin(gradDescMin(:,end)==min(gradDescMin(:,end)),1:end-1);
pOptimal = pOptimal(1,:);

[~, F, G] = l2errorFMS(pOptimal,formletRep, testShape, L, lambda, inShape, isotropic);

%% save results
formlet.z = complex(pOptimal(1), pOptimal(2));
formlet.s = pOptimal(3);
if ~isotropic
  formlet.k = pOptimal(4);
  formlet.theta = pOptimal(5);
end
formlet.a = getOptimalGain(pOptimal, formletRep, testShape, L, lambda);