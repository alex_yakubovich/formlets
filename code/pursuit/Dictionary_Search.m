function selectionScore = Dictionary_Search(testShape,formletRep, dictionary, lambda, L, inShape)

%create error and gain parameter arrays
Z_Map_Size = size(dictionary.Z_Map);
selectionScore = zeros(Z_Map_Size(1), Z_Map_Size(2), length(dictionary.scales), length(dictionary.k), length(dictionary.theta0));

n = length(testShape); 
dictionary.Z_Map = repmat(dictionary.Z_Map, [1,1,n]);

%translate current approximation
gammaKm1 = repmat(reshape(formletRep, [1 1 n]), [Z_Map_Size(1), Z_Map_Size(2)]);
gammaKm1zeta =  gammaKm1 - dictionary.Z_Map;
gammaObs = repmat(reshape(testShape, [1 1 n]), [Z_Map_Size(1), Z_Map_Size(2)]);                         

theta = angle(gammaKm1zeta);
r = abs(gammaKm1zeta);

g0 = gammaKm1zeta./r ;

formletRep2 = repmat(reshape(formletRep, [1 1 n]), [Z_Map_Size(1), Z_Map_Size(2), 1]);
for scale = dictionary.scales
  gainL = -scale/(2*pi);
  gainU = .1956*scale;
  
  gk = g0 .* sin(2*pi*r/scale) .* exp(-(r.^2)/scale^2);    
  for theta0 = dictionary.theta0
    cosT = cos(theta-theta0);  
    for k = dictionary.k
      
      %compute form bumping vectors
      gk = gk .* exp(k *(cosT -1));
      
      %  optimal gain computation
      Cks = 0.2568*pi*scale^2*exp(-2*k)*besseli(0,2*k);
      
      optimalGain = sum(IP(gammaObs-gammaKm1, gk),3)./(sum(IP(gk,gk),3) + lambda*Cks/L);            
      
      optimalGain((optimalGain>=gainU)) = gainU;
      optimalGain((optimalGain<=gainL)) = gainL;
      
      formed = formletRep2 + repmat(reshape(optimalGain, [Z_Map_Size 1]), [1, 1, n]) .* gk;
      formed = gammaObs - formed;                                                                                                                              
      
      %form sum-of-squares objective function
      selectionScore(:,:, dictionary.scales==scale, dictionary.k==k, dictionary.theta0==theta0) = ...
      L * sum(IP(formed,formed),3) + lambda*Cks*optimalGain.^2;                  
    end
  end
end

% force locations to lie inside contour
if inShape
    xyIn = inpolygon(real(dictionary.Z_Map(:,:,1)), imag(dictionary.Z_Map(:,:,1)),real(formletRep), imag(formletRep));
    xyIn = repmat(xyIn, [1 1 length(dictionary.scales) length(dictionary.theta0) length(dictionary.k)]);
    selectionScore(~xyIn) = Inf;
end