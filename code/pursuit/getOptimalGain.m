function optimalGain = getOptimalGain(p, gammaKm1,gammaObs, L, lambda)
%Find the optimal gain parameter

% INPUT
% p=[r,c,s,k,theta] or p=[r,c,s] vector of (an)isotropic formlet parameters
% gammaObs - nx1 complex vector - observed contour
% gammaKm1 - nx1 complex vector - current approximation

% OUTPUT
% optimalGain - 1x1 double - gain parameter minizing residual l2 error

gammaKm1zeta = gammaKm1-complex(p(1),p(2));

if length(p)>3
  adf = exp(p(4) * cos((angle(gammaKm1zeta) - p(5))))/exp(p(4));
else
  adf=1;
end

rdf = exp(-(abs(gammaKm1zeta).^2)./(p(3).^2)) .* sin(2*pi*abs(gammaKm1zeta)/p(3));
gk = rdf .* adf .* ((gammaKm1zeta)./abs(gammaKm1zeta));

if lambda>0  
 % avLen = .5*(mean(abs(gammaKm1(2:end) - gammaKm1(1:end-1))) ...
   % + mean(abs(gammaObs(2:end) - gammaObs(1:end-1))));  
  %res = mean(abs(gammaObs - gammaKm1));
  %res = sum(abs(gammaObs - gammaKm1));
  %res = sqrt(sum(real(res.^2) + imag(res.^2)));
  
  %L = res_old*avLen;
  
  C = 0.2568*pi*p(3)^2*exp(-2*p(4))*besseli(0,2*p(4)); %change avLen
  
  optimalGainNumerator = sum(real(gammaObs-gammaKm1).*real(gk) + imag(gammaObs-gammaKm1).*imag(gk));
  optimalGainDenominator= sum(real(gk).^2 + imag(gk).^2) + lambda*C/L;
%   optimalGainNumerator = sum(weights .* (real(gammaObs-gammaKm1).*real(gk) + imag(gammaObs-gammaKm1).*imag(gk)));
%   optimalGainDenominator= sum(weights.*(real(gk).^2 + imag(gk).^2)) + lambda*C/L;
  optimalGain = optimalGainNumerator./optimalGainDenominator;
    
else
  optimalGainNumerator = sum(real(gammaObs-gammaKm1).*real(gk) + imag(gammaObs-gammaKm1).*imag(gk));
  optimalGainDenominator= sum(real(gk).^2 + imag(gk).^2);
  optimalGain = optimalGainNumerator./optimalGainDenominator;
end

if optimalGain>.1956*p(3), optimalGain=.1956*p(3);
elseif optimalGain<-p(3)/(2*pi), optimalGain = -p(3)/(2*pi);
end