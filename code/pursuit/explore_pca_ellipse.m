figDir = '~/Dropbox/Thesis/Documentation/cvpr2014/followup/pursuit/';
load('~/Dropbox/Thesis/formlets-release/data/preferences.mat')

for file = 1:100
    %read image
    fileName = pref.dataset.filelist(file).name; %get filename
    image = imread([pref.dataset.directory fileName]);
    
    datum = extract_contour(image, pref.visibleSamples, pref.n, pref.offset(file), pref.coordinateSystem);
    
    shape = datum.target.entire;    
    
    %find principal components
    [pc, lambda] = eig(cov([real(shape), imag(shape)]));    
    [lambda, i] = sort(diag(lambda), 'descend'); %sort eigenvalues in descending order
    pc1 = pc(:, i(1)); %largest principal component = major axis
    
    % generate principal components ellipse
    phi = atan(pc1(2)/pc1(1)); %orientation determined by major axis
    a = sqrt(lambda(1)); %length of major axis
    b = sqrt(lambda(2)); %length of minor axis
    
    n = 256;
%     n=128;
    t = linspace(0, 2*pi, n)';
    
    ellipse = mean(shape) + ...
        complex(a*cos(t)*cos(phi) - b*sin(t)*sin(phi), ...
        a*cos(t)*sin(phi) + b*sin(t)*cos(phi));
        
    %plotter(shape, ellipse, [], 1);
       
    %visualization
    clf
    plot(shape);
    hold on;
    plot(shape, '.', 'MarkerSize', 15);
    plot(mean(shape), 'rx', 'MarkerSize', 15);
    axis image;
    plot(ellipse, 'b');
    saveas(gcf, [figDir, 'shape', num2str(file)], 'epsc');
    
    %reparameterize ellipse 
    
    ix = cm(shape, ellipse, 1, 0);
        
     shape_struct = struct('entire', shape, 'visible', shape, 'occluded', []);
     ellipse_struct =  struct('entire', ellipse, 'visible', ellipse, 'occluded', []);      
     plotter(shape_struct, ellipse_struct, [], 1, 0, ix)
     saveas(gcf, [figDir, 'shape', num2str(file), 'cm'], 'epsc');
     
     
     ellipse_repam  = reparameterize(ix, shape_struct, ellipse_struct);
     plotter(shape_struct, ellipse_repam, [], 1);
     saveas(gcf, [figDir, 'shape', num2str(file), 'cm-1-to-1'], 'epsc');
    
     close all
end