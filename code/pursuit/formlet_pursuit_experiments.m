  dataDir = '../../Data/';
  
%% 10% Completion
occlusion = 0.1;
%pref = define_preferences(occlusion);
%save([dataDir 'preferences'], 'pref');

load([dataDir 'preferences'], 'pref');
pref.init = 'circle';

%shapelets
pref.coordinateSystem = 'unit-variance';
shapelet_pursuit(pref)

% TO DO

% isotropic formlets
pref.fileName = [dataDir, '10%-occlusion-isotropic-formlets'];
pref.optPair = 'match-first-iteration';
pref.isotropic = 1;
formlet_pursuit(pref);


% oriented formlets
pref.fileName = [dataDir, '10%-occlusion-oriented-formlets'];
pref.optPair = 'match-first-iteration';
formlet_pursuit(pref);

%oriented formlets + cm
pref.fileName = [dataDir, '10%-occlusion-oriented-cm-formlets'];
formlet_pursuit(pref)

%oriented formlets + cm + regularization
pref.lambda = .003942; %third pass
pref.fileName = [dataDir, '10%-occlusion-oriented-cm-regularized-formlets'];
formlet_pursuit(pref)


%% 30% Completion
occlusion = 0.3;
pref = define_preferences(occlusion);
dataDir = '../../Data/';
%save([dataDir 'preferences-30%-occlusion'], 'pref');

load([dataDir 'preferences-30%-occlusion'], 'pref');
pref.init = 'circle';

%shapelets
% TO DO

%isotropic formlets
pref.fileName = [dataDir, '30%-occlusion-isotropic-formlets'];
pref.optPair = 'match-first-iteration';
pref.isotropic = 1;
formlet_pursuit(pref);

%oriented formlets
pref.fileName = [dataDir, '30%-occlusion-oriented-formlets'];
pref.optPair = 'match-first-iteration';
formlet_pursuit(pref);

%oriented formlets + cm
pref.fileName = [dataDir, '30%-occlusion-oriented-cm-formlets'];
formlet_pursuit(pref)

%oriented formlets + cm + regularization
pref.lambda = .003942; %third pass
pref.fileName = [dataDir, '30%-occlusion-oriented-cm-regularized-formlets'];
formlet_pursuit(pref)

%% Convergence