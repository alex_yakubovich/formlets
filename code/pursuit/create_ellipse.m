%function ellipse = create_ellipse(par, resolution, phi_start, phi_end)
function ellipse = create_ellipse(par, phi)

%SetDefaultValue('resolution', 128);
%SetDefaultValue('phi_start', 0);
%SetDefaultValue('phi_end', 2*pi);

%phi = linspace(min(phi_start, phi_end), max(phi_start, phi_end), resolution) 

SetDefaultValue('phi', linspace(0,2*pi, 128));

centre = complex(par.x0, par.y0);
a = par.a;
b = par.b;
theta = par.theta;

ellipse = centre + complex(a*cos(phi)*cos(theta) - b*sin(phi)*sin(theta), ...
    a*cos(phi)*sin(theta) + b*sin(phi)*cos(theta)).';
