function [ contour ] = shapelet_basis(Lambda, Mu, Sigma, Resolution)
%SHAPELET_BASIS Shapelet generating function.
%   SHAPELET_BASIS(L, M, S, n) returns the contour of a shapelet with a
%   resolution of n using the parameters L, M, and S.
%
%   Here, S denotes the attenuation parameter.  If S is zero, the strict
%   gabor basis of Dubinskiy and Zhu is used.  Otherwise, a cosine
%   attenuated ellipse basis is substituted.
%
%   See also shapelet_dictionary, shapelet_shape, shapelet.

    error(nargchk(4, 4, nargin, 'struct'))
    t = (0:Resolution-1)/(Resolution);

    %select basis
    if Sigma ~= 0
        %cosine^sigma attenuated elipse
        contour = (1 + cos(2*pi/Lambda.*(t - 1/2))).^(Sigma) .* ((1 + cos(2*pi/Lambda.*(t - 1/2)))) + ...
                   1i*(1 + cos(2*pi/Lambda.*(t - 1/2))).^(Sigma) .* (sin(2*pi/Lambda.*(t - 1/2)));
        contour = contour';
        contour(t < 1/2-Lambda/2 | t > 1/2+Lambda/2) = 0;
    
    elseif Lambda ~= 0
        %strict gabor basis
        contour = (exp(-((t-1/2).^2)/(2*(Lambda.^2)))) .* ((cos(2*pi/Lambda.*(t - 1/2)))) + ...
                   1i*((exp(-((t-1/2).^2)/(2*(Lambda.^2)))) .* (sin(2*pi/Lambda.*(t - 1/2))));
        contour = contour';
        contour(t < 1/2-1.5*Lambda | t > 1/2+1.5*Lambda) = 0;
    
    else
        %ellipse
        contour = exp(1i*((2*pi/Resolution)*(1:Resolution)))';
        
    end

    
    %center at mu
    contour = [contour(t >= (1/2),:); contour(t < (1/2),:) ];
    contour = [contour(t > (1-Mu),:); contour(t <= (1-Mu),:) ];

    %normalize shapelet
    contour = complex(real(contour)/sqrt(sum(real(contour).^2)), imag(contour)/sqrt(sum(imag(contour).^2)));
end

