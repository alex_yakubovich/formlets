
function [formletRep1,mapping] = reparameterize(CM_match, formletRep)
% Reparameterize(CM_match, testShape, formletRep, occludedSamples, viz) reparameterizes
% formletRep, adding or removing points when there is a non 1-1
% correspondence to testShape. The correspondence between the two contours
% is given by CM_match.

%If testShape is partially occluded, we force a 1-1 match to the occluded region, sampling as many points as required.

% Algorithm:
% We traverse testShape. If a point on testShape is matched to n>1 points
% on formletRep, we interpolate n points between its neighbours. If n>1
% points on testShape are matched to 1 point on formletRep, we replace
% these n points by their midpoint.

%mapping is an nx2 matrix storing the mapping of points between formletRep and formletRep1.
%mapping(1,:) = [i,j] means point j on formletRep is the parent of point
%i on formletRep1

%CM_match = [testShape, formletRep]
%mapping = [new, old]


%n = length(testShape.entire);
n = length(formletRep);
t = 1; %index on CM_match

mapping = []; %indices before and after reparamaterization [new,old]
formletRep1 = [];
%bug when there is a n-1 match on the first point

%% correct matchings on visible portion
while t <= length(CM_match)

    % check  if point t on formletRep is matched to n>1 points on testhape
    p2 = CM_match(t,2);
    reps2 = CM_match(:,2) == p2; 
    nreps2 = sum(reps2); %number of times point t repeats    
    
    if nreps2>1 %point on formletRep repeats -> interpolate b/w neighbours
      
      if isempty(mapping) %t==1
        mapping = [mod((p2:p2+nreps2-1)-1, n)'+1, repmat(p2, nreps2,1)]; %??
      else
        mapping = [mapping; ... %record mapping
          [mod((mapping(end,1)+1:mapping(end,1)+1+nreps2-1)-1, n)'+1, ...
          repmat(mod(mapping(end,2), n)+1, nreps2,1)]];

      end
      t2 = p2;
       
       %neighbour 1              
       if t2>1 % repeating point is not the first on the contour
          if ~isempty(formletRep1) 
            prev = formletRep1(end); %last point on the model
          else
            prev = formletRep(t2-1); 
          end           
       else
          prev = formletRep(t2);
          %prev = formletRep(end);
       end
       
       %neighbour 2
       if t2<n 
            next = formletRep(t2+1);
       else          
           next = formletRep(t2);
       end
                  
        s = linspace(0,1,nreps2+2); %add 2 for flanking points
        s = s(2:(end-1)); %inner points                      
        
        fnew = (prev * (1-s) + next * s).';
        %fnew = (formletRep.entire(prev)*(1-s) + formletRep.entire(next)*s).';        
        
        t = t+nreps2;        
         
    else     % point on formletRep does not repeat
        %check if point on target repeats
        p1 =  CM_match(reps2,1); 
        reps1 = CM_match(:,1) == p1;
        nreps1 = sum(reps1);
                
        if nreps1 > 1 %point on testshape repeats -> take midpoint   
          p1f = CM_match(CM_match(:,1)==p1,2); %points on formletRep matched to p1
          fnew = mean(formletRep(p1f));
          
          t = t +  nreps1; % proceed to next point (which hasn't been deleted)                   
          
          if isempty(mapping)  %t==1          
           mapping = [mapping; repmat(p2, nreps1,1), p1f];
          else
            mapping = [mapping; ... %record mapping
              [repmat(mod(mapping(end,1),n)+1, nreps1,1), mod((mapping(end,2)+1:mapping(end,2)+1+nreps1-1)-1, n)'+1]];
              %[repmat(mapping(end,1)+1, nreps1,1), mod((mapping(end,2)+1:mapping(end,2)+1+nreps1-1)-1, n)'+1]];
%               [repmat(p2, nreps1,1), p1f]];            
          end
          
        else %1:1 matching
               if t==1           
                mapping = [mapping; p2, p2];
               else
                 mapping = [mapping; mod(mapping(end,:), n)+1];
                 %mapping = [mapping; mapping(end,:)+1];
           end  
          
          
          fnew = formletRep(CM_match(t,2));                                
           t = t+1;    
           
        end                                            
    
    end    
    formletRep1 = [formletRep1; fnew];
    %mapping
end

%mapping(:,1) = mod(mapping(:,1)-CM_match_vis(1,2), n)+1; %undo rotation on occluded


% mapping_occ = [mapping(end,1)+1:(mapping(end,1)+occludedSamples); mapping(end,2)+1:(mapping(end,2)+occludedSamples)]';
% mapping_occ = mod(mapping_occ-1, n)+1;
% mapping = [mapping; mapping_occ];

%% matching on occluded part

%CM_match(end,2) CM_match(1,2)

% If there is a point on the formletRep matched to both visible and
% occluded points on targetShape, remove it from the completion

% (!) This leads to a bug  if there is only one point on formletRep matched
% to all occluded points and some visible points on testShape

% try
% if occludedSamples > 0    
% %      while ~isempty(CM_match_occ) && (CM_match_occ(1,2)==CM_match_vis(end,2)) 
% %          CM_match_occ = CM_match_occ(2:end,: );
% %      end 
%      
%     %completion = formletRep.entire(unique(CM_match_occ(:,2), 'stable')); % unique points on formletRep matched to occluded region        
%     %t = linspace(0, 1, occludedSamples);
%     
%     % unique points on formletRep matched to occluded region        
%     completion = formletRep(...
%       [CM_match_vis(end,2);          
%       CM_match_occ(:,2);
%       CM_match_vis(1,2)]);
%     completion = unique(completion, 'stable');       
%     t = linspace(0, 1, occludedSamples+2);
%     t = t(2:end-1);
%     
%     formletRep1.occluded = conj(interp1([0; cumsum(abs(diff(completion)))]/max(cumsum(abs(diff(completion)))), completion, t)'); %interpolate    
%     if viz; plot(formletRep1.occluded, '.b', 'MarkerSize', 10); end    
% else
%     formletRep1.occluded = [];
% end
% catch
%   keyboard
% end
%formletRep1.entire = [formletRep1.visible; formletRep1.occluded];


