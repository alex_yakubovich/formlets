function [formletRepRot, bestRot] = match_by_rotation(targetVisible, formletRep)
% [formletRepRot, bestRot] = match_by_rotation(targetVisible, formletRep)
% searches across all the cyclic shifts of formletRep, and finds the one
% minimizing the l2 norm of the residual, targetVisible - formletRep.  

%convert to vector if needed


%% BUG HERE!!!! %%
if isstruct(formletRep)    
    formletRep1 = formletRep.entire;
    %formletRep1 = formletRep.visible;
    clear formletRep;
    formletRep = formletRep1;
else
    structFlag = false;
end
    
visibleSamples = length(targetVisible);
n = length(formletRep);

viz=false;

for rot = 1:n
    
    formletRepRot = circshift(formletRep, rot);
    formletRepVisibleRot = formletRepRot(1:visibleSamples);
    
    e = targetVisible - formletRepVisibleRot;
    
    err(rot) = sum(real(e).^2 + imag(e).^2);
    
    if viz; plotter(targetVisible, formletRepVisibleRot, [], 1); end
end

[~, bestRot] = min(err);

clear formletRepRot;
formletRepRot.entire = circshift(formletRep, bestRot);
formletRepRot.visible = formletRepRot.entire(1:visibleSamples);
formletRepRot.occluded = formletRepRot.entire(visibleSamples+1:n);
