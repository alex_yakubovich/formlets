 
function formlet_data = formlet_pursuit(pref, init)

if nargin==0; pref = define_preferences(0.1,0); pref.save=0; pref.init = 'ellipse-analytical-optimal'; end 

figDir = '~/Dropbox/Thesis/Documentation/cvpr2014/followup/pursuit/optimal-ellipse/'; 

formlet_data = cell(1,pref.nfiles);  

%shape 35,39,119, 179 breaks down 
 %for file = 119
for file = 1:pref.nfiles 
   
    %read image and extract shape
    fileName = pref.dataset.filelist(file).name; 
    image = imread([pref.dataset.directory fileName]);    
   
    datum = extract_contour(image, pref.visibleSamples, pref.n, pref.offset(file), pref.coordinateSystem);
    datum.name = fileName;
    
    %formletRep = initialize_pursuit(datum.target.visible, datum.region, pref.visibleSamples, pref.n, pref.init, pref.optPair);                               
    
    if nargin==2
        formletRep = init{file};
    else
        formletRep = initialize_pursuit(datum.target, datum.region, pref.visibleSamples, pref.n, pref.init, pref.optPair);
    end
    
    datum.formlets.init = formletRep;
    
    dictionary = build_dictionary(pref, datum.target);
    
    formlet=[];

%load('debug-shape-252.mat')
    
    for element = 1:pref.niterations
        
        tic;
                
%         if strcmp(pref.optPair, 'cyclic-shifts') && element==1 && strcmp(pref.init,'ellipse-analytical-optimal')                                
        if element==1 && strcmp(pref.init,'circle')                                
            formletRep = match_by_rotation(datum.target.visible, formletRep);
            datum.formlets.init = formletRep;
        end    
            
        if (strcmp(pref.optPair, 'CM') && element > 1) || (strcmp(pref.init,'circle') && element > 1)
            [ix, rot] = cm(datum.target.visible, formletRep.visible, pref.occlusion==0);                                                  
            %[ix, rot] = cm(datum.target.visible, formletRep.entire, pref.occludedSamples, 1);                                                  
            %plotter(datum.target, formletRep, [], 1, 0, ix);    
           [formletRep.visible,mapping]  = reparameterize(ix, formletRep.visible);           
           formletRep.entire = [formletRep.visible; formletRep.occluded];
           %[formletRep,mapping]  = reparameterize(ix, formletRep.entire, pref.occludedSamples);           
           %[formletRep,mapping]  = reparameterize2(ix, datum.target.entire, formletRep.entire);                                    
            formletRepAfterRepam = formletRep;             
        end                
        
        if pref.plot.iter            
         plotter(datum.target, formletRep, formlet, pref.plot.res, pref.plot.grid);                           
         if pref.plot.save; saveas(gcf, [figDir,sprintf('pursuit-update-correspondence-iteration-%0.2i', element)], 'epsc'); end
        end
        
        saveas(gcf, ['init-file-', num2str(file)], 'epsc');
        
        %formlet = dictionary_descent(datum.target.visible, formletRep.visible, pref, dictionary, pref.lambda); %regularized       
        
        L = Find_Fidelity_Term_Constant(datum.target.visible, formletRep.visible,pref.visibleSamples);
        selectionScore = Dictionary_Search(datum.target.visible,formletRep.visible, dictionary, pref.lambda, L, pref.inShape);
        formlet = Selective_Descent(datum.target.visible, formletRep.visible, selectionScore, dictionary, pref.lambda, L, pref.isotropic, pref.optimizationParameters, pref.inShape);                
                
        
        formletRep = formlet_basis(formletRep, formlet);        
        
        %store formlet and corresponding contours into datum        
        
        if strcmp(pref.optPair, 'CM') && element > 1           
          datum.formlets.rotation(element) = rot;
          datum.formlets.repamMapping{element} = mapping;
          datum.formlets.formletRepAfterRepam{element} = formletRepAfterRepam;
        end
        datum.formlets.formletRep{element} = formletRep;
        datum.formlets.element{element} = formlet;
        datum.formlets.timer{element} = toc;                
        
        if pref.plot.iter            
         plotter(datum.target, formletRep, formlet, pref.plot.res, pref.plot.grid);                           
         if pref.plot.save; saveas(gcf, [figDir,sprintf('pursuit-update-formlet-iteration-%0.2i', element)], 'epsc'); end
        end
        
    end
    
    %% save results
    datum.runningtime = sum([datum.formlets.timer{:}])/60; %save final running time    
    formlet_data{file} = datum;
   
    disp(['Processed ' datum.name ', file ' num2str(file) ' in ' num2str(datum.runningtime) ' minutes.']);    
    
    if pref.save
      save(pref.fileName, 'formlet_data', 'pref');
    else
      display('Not saving data!');
    end   
end