
function m = buildMesh(shape1, shape2, t, resolution) 

SetDefaultValue('t', .2);
%SetDefaultValue('resolution', 100);
SetDefaultValue('resolution', 30);


if ~exist('shape2', 'var')
  shape2=shape1;
end
  


xmin=min(real([shape1;shape2]));
xmax=max(real([shape1;shape2]));

ymin=min(imag([shape1;shape2]));
ymax=max(imag([shape1;shape2]));

x=linspace(xmin-t*(xmax-xmin),xmax+t*(xmax-xmin), resolution);
y=linspace(ymin-t*(ymax-ymin),ymax+t*(ymax-ymin), resolution);

[X,Y] = meshgrid(x,y);
m = X+1i*Y;