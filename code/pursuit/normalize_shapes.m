function [datum, shapeletRep] = normalize_shapes(shapeletRep, datum, norm_type)

switch norm_type
    case 'unit-variance'
                
        t = datum.target.entire - mean(datum.target.entire);
        
        %target.entire = t/sqrt(sum(real(t).^2))'
        target.entire = 100*complex(real(t)/sqrt(sum(real(t).^2)), imag(t)/sqrt(sum(imag(t).^2)));
        target.visible = target.entire(1:115);
        target.occluded = target.entire(116:128);
        datum.target = target;
        
        s = shapeletRep.entire - mean(shapeletRep.entire);
        shapeletRep.entire = 100*complex(real(s)/sqrt(sum(real(s).^2)), imag(s)/sqrt(sum(imag(s).^2)));
        shapeletRep.visible = shapeletRep.entire(1:115);
        shapeletRep.occluded = shapeletRep.entire(116:128);
        
        
        
        
        
        
        %norm_factor = 100;
                       
        %norm_factor = std(abs(datum.target.visible - mean(datum.target.visible)));                
%        centroid = mean(datum.target.visible);        
%         
%         shapeletRep.entire = shapeletRep.entire - centroid
%         shap
%shapeletRep.visible = shapeletRep.visible - centroid;
%         shapeletRep.occluded= shapeletRep.occluded - centroid;
%         
%         datum.contour = datum.contour - centroid ; %normalize shape so that max(height,width) =1
%         datum.target.entire = datum.target.entire - centroid;
%         datum.target.visible= datum.target.visible - centroid;
%         datum.target.occluded = datum.target.occluded - centroid;
                      
    case 'unit-max-dimension'
        n = length(shapeletRep.entire);
        
        x = real(datum.contour);
        y = imag(datum.contour);
        
        bR= repmat([x, y],1,n);
        MR=reshape([x'; y'], 1, 2*n); MR = repmat(MR, n, 1);
        dt=(MR-bR).^2; D1= sqrt(dt(:,1:2:end-1) + dt(:, 2:2:end) ); %  element(r,c) is the distance of rth point on a from cth point on b
        dt = abs(MR-bR);
        Dx = dt(:,1:2:end-1); %Dx[i,j] = |xi - xj|
        Dy = dt(:, 2:2:end); %Dy[i,j] = =|yi - yj|
        
        norm_factor = max(max(Dx(:)), max(Dy(:)));
end

% datum.norm_factor = norm_factor;
% 
% datum.contour = datum.contour/norm_factor; %normalize shape so that max(height,width) =1
% datum.target.entire = datum.target.entire/norm_factor;
% datum.target.visible= datum.target.visible/norm_factor;
% datum.target.occluded = datum.target.occluded/norm_factor;
% 
% 
% if ~isempty(shapeletRep)
%     shapeletRep.entire = shapeletRep.entire/norm_factor;
%     shapeletRep.visible = shapeletRep.visible/norm_factor;
%     shapeletRep.occluded= shapeletRep.occluded/norm_factor;
% end