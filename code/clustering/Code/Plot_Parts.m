
% visualize the parts graph by applying formlets in a subtree to the embryo


%addpath('../../Regularization_Experiment/Code/Pursuit/');
addpath('../../formlets-release/code/pursuit/')
close all;

if ~exist('formlet_data', 'var')
    %load('../../Regularization_Experiment/Data/no-occlusion/formlets-lambda-0.003942-full.mat');
    load('../../formlets-release/data/0%-occlusion-oriented-cm-regularized-formlets-optimal-ellipse.mat')
end
load('../Data/correlation-matrix-ellipse');
%shapeSample = [1, 158, 179];
%shapeSample = 1:50;
shapeSample = [4, 163];

%shape 163 - rhino 2
%figRootDir = '../Figures/';
figRootDir = '~/Dropbox/Thesis/Documentation/thesis-manuscript/clustering/fig/ellipse/';

colors = linspecer(2);

for file = shapeSample %1:%pref.nfiles
    file
    figDir = [figRootDir 'trees/shape' num2str(file) '/'];
    if exist(figDir, 'dir')==0; mkdir(figDir);end
    
    %% compute part trees
    partTrees = Cluster_Formlets(correlation(:,:,file), 0);
    nTrees = length(partTrees);
    
    %% plot trees
    figure;
    Plot_Tree_Color(partTrees{1}, correlation);
    saveas(gcf, [figDir, 'tree-topology'], 'epsc')
    
    %% plot formlets in every subtree
    dtree = depthtree(partTrees{1});
    nodesLevel1 = find(cell2mat(dtree.Node)==1)';
    
    subTreeCount =0 ;
    
    for subTreeRoot = nodesLevel1 %all children of 1
        subTreeCount = subTreeCount + 1;
        
        %get children
        subtree = partTrees{1}.subtree(subTreeRoot);
        subtreeNodes= cell2mat(subtree.Node)';
        subtreeNodes = sort(subtreeNodes);
        
        %plot root
        %m = plotter_regions(formlet_data{file}.target, formlet_data{file}.formlets.formletRepAfterRepam{2},formlet_data{file}.formlets.element{1}, false, true, [], [], 'r');
        %plotter_regions(formlet_data{file}.target, formlet_data{file}.formlets.formletRepAfterRepam{2},[], false, false, [], [], colors(1,:));
        m = buildMesh(formlet_data{file}.target.entire);
        formletRepRoot = formlet_data{file}.formlets.formletRepAfterRepam{2};
        part = formletRepRoot;
        hold on;
        
        %plot children
        
        for f = subtreeNodes
            part = formlet_basis(part, formlet_data{file}.formlets.element{f});
            %m = plotter_regions(formlet_data{file}.target, part,formlet_data{file}.formlets.element{f}, false, true, [], [], colors(2,:),m);
            m = plotter(formlet_data{file}.target, part,formlet_data{file}.formlets.element{f}, false, true, [], [], colors(2,:),m, 3);
            hold on;
            %plot([formletRepRoot.entire; formletRepRoot.entire(1)], 'Color', colors(1,:));
            plot([formletRepRoot.entire; formletRepRoot.entire(1)], 'Color', colors(1,:), 'LineWidth', 3);
            title(['Subtree rooted at formlet ' num2str(subTreeRoot)], 'FontSize', 24);
        end
        saveas(gcf, [figDir sprintf('subtree%i-formlets.eps',subTreeCount)], 'epsc')
        
    end
    
end