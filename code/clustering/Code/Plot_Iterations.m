% load formlet data and plot the model and target at each iteration. The
% color represents the action of a formlet at every point on the model.

load('../../formlets-release/data/0%-occlusion-oriented-cm-regularized-formlets-optimal-ellipse.mat')
load('../Data/correlation-matrix-ellipse.mat');

%figRootDir = '../Figures/pursuit/';
figRootDir = '~/Dropbox/Thesis/Documentation/thesis-manuscript/clustering/fig/ellipse';

addpath('~/Dropbox/Thesis/formlets-release/code/pursuit/');

%shapeSample = 4%160:170%[65, 88,106,126]; %[1, 158, 179];
shapeSample = [4, 163];
for file = shapeSample%1:pref.nfiles
  fprintf('file %i \n', file);
  figDir = [figRootDir '/pursuit/shape' num2str(file) '/'];
  
  if exist(figDir, 'dir')==0; mkdir(figDir);end
  
  clf;
  target = formlet_data{file}.target.entire;
  target = [target; target(1)];
  
  init = formlet_data{file}.formlets.init.entire;
  init = [init; init(1)];
  
  m = build_mesh(init, target, [], 30);
  plot_mesh(m)
  %plotMesh(m)
  hold on;
  %plot(target, 'Color', [.5 .5 .5], 'LineWidth', 1);
  plot(target, 'Color', [.5 .5 .5], 'LineWidth', 3);
  
  %plot initialization
  
  plot(init, 'LineWidth', 2);
  title('0', 'FontSize', 16, 'FontWeight', 'bold');
  axis image;
  axis off;
  saveas(gcf, [figDir 'it00'], 'epsc')
  
  for el = 1:pref.niterations
    clf;
    
    fshape = formlet_data{file}.formlets.formletRep{el}.entire; %apply formlet to model
    
    fshape = [fshape; fshape(1)];
    
    %deform mesh and plot
    %m = buildMesh(fshape, target);
    m = build_mesh(fshape, target, [], 30);
    m = formlet_basis(m, formlet_data{file}.formlets.element{el});
    hold on;
    plot_mesh(m);
    %plotMesh(m);
    
    %plot(target, 'Color', [.25 .25 .25], 'LineWidth', 1)
    plot(target, 'Color', [.3 .3, .3], 'LineWidth', 3)
    
    A = action(el, :,file)';
    A = [A; A(1)];
    color_line(real(fshape), imag(fshape), A);
    
    title(num2str(el), 'FontSize', 16, 'FontWeight', 'bold');
    axis image;
    axis off;
    saveas(gcf, sprintf([figDir 'it%0.2d'], el), 'epsc')
  end
  
end