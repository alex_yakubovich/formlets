function partTrees = Cluster_Formlets(correlation, threshNewTree, verbose)

%function partTrees = Cluster_Formlets(correlation, threshNewTree) computes the part
%trees, a cell array of trees, given a nxn formlet correlation matrix. A
%new tree is added if the correlation with all formlets in the
%forest is less than threshNewTree.

%Note: the tree package is required
addpath('tree/')

SetDefaultValue('threshNewTree',.5); %threshold for when to start a new tree
SetDefaultValue('verbose',false); %threshold for when to start a new tree

[~, n] = size(correlation);

%initialization
coord = [1, 1, 1]; %[formlet #, node #, tree #]
partTrees = cell(1);
partTrees{1} = tree(1);

for formlet = 2:n
  
  [maxCorr, imax] = max(correlation(formlet, 1:formlet-1));
  
  if maxCorr < threshNewTree %start a new tree
    
    partTrees{end+1} = tree(formlet);
    coord(end+1,: ) = [formlet, 1, length(partTrees)];
    
  else %add a leaf to existing tree
    
    treeID = coord(imax, 3);
    parentID = coord(imax, 2);
    
    [partTrees{treeID}, leafID] = partTrees{treeID}.addnode(parentID, formlet); %add a leaf
    coord(end+1,:) = [formlet, leafID, treeID];
    
  end  
  if verbose %print tree in command window
    fprintf('** iteration %i ** \n', formlet);
    Plot_Forest(partTrees)
    %coord
  end
  
end

Plot_Forest(partTrees)
    
%partTrees{1}.plot