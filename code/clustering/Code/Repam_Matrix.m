function M = Repam_Matrix(ix)

%given a mapping betweeen the contour before and and after
%reparamaterization, represent it as a nxn matrix

if nargin==0 %example
  ix = [1,1; ...
    2,2; ...
    3,2; ...
    4,3;
    4,4];
end

n = max(ix(:));

M = zeros(n);

for i =1:length(ix)
  M(ix(i,1), ix(i,2)) = 1; 
end

M = M./repmat(sum(M,2), [1,n]);



