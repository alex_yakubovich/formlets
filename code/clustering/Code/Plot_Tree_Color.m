function Plot_Tree_Color(tree, correlation) 

ucorrelation = linspace(0,1, 20);
colors = linspecer(length(ucorrelation));

%tree = partTrees{t};

clf;
%[vlh hlh tlh] = plot(tree);
%set( tlh.get(1), 'FontSize' , 18, 'FontWeight', 'bold');
axis off;

col=[];
for nodeID = 2:length(tree.Node)  
  child = tree.Node{nodeID};
  parent = tree.Node{getparent(tree, nodeID)} ;
  
  corr = correlation(parent, child);
 
  [~, ix] = min(abs(corr - ucorrelation));
  ix = ix(1);
  col = [col; colors(ix,:)];
  %set( tlh.get(nodeID), 'Color' , colors(ix,:) );
  %set( tlh.get(nodeID), 'FontSize' , 18, 'FontWeight', 'bold');
end


%% plot tree

treeVec = tree.Parent';
%trimtreeplot(treeVec, 'ro', 'b')
%trimtreeplot(treeVec, 'ro', 'b', col)
%trimtreeplot(tree.Parent', 'ro', 'b', col, tree.Node')
trimtreeplot(tree.Parent', tree.Node', col)
colorbar;
caxis([0 1]);

[x,y] = trimtreelayout(treeVec);
name1 = cellstr(num2str(cell2mat(tree.Node)));
text(x(1,:), y(1,:), name1, 'VerticalAlignment','bottom','HorizontalAlignment','right', 'FontWeight', 'bold', 'FontSize', 14)

