function partTrees = Cluster_Formlets_No_Thresh(correlation, action, verbose)

%function partTrees = Cluster_Formlets(correlation, threshNewTree) computes the part
%trees, a cell array of trees, given a nxn formlet correlation matrix. A
%new tree is added if the correlation with all formlets in the
%forest is less than threshNewTree.

%Note: the tree package is required


% requires the tree package
SetDefaultValue('verbose',false); %threshold for when to start a new tree

[~, n] = size(correlation);

%initialization
coord = [1, 1, 1]; %[formlet #, node #, tree #]
partTrees = cell(1);
partTrees{1} = tree(1);

uniFormlet = ones(length(action),1);

for k = 2:n
  
  [maxCorr, imax] = max(correlation(k, 1:k-1));
  
  corrWithUniform = Cosine_Distance(uniFormlet, action(k,:)')
  
  if maxCorr < corrWithUniform %start a new tree
    
    partTrees{end+1} = tree(k);
    coord(end+1,: ) = [k, 1, length(partTrees)];
    
  else %add a leaf to existing tree
    
    treeID = coord(imax, 3);
    parentID = coord(imax, 2);
    
    [partTrees{treeID}, leafID] = partTrees{treeID}.addnode(parentID, k); %add a leaf
    coord(end+1,:) = [k, leafID, treeID];
    
  end  
  if verbose %print tree in command window
    fprintf('** iteration %i ** \n', k);
    Plot_Forest(partTrees)
    %coord
  end
  
end

Plot_Forest(partTrees)   