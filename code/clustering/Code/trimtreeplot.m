function trimtreeplot(p, nodes, colors, marker)
% TRIMTREEPLOT A modified standard function TREEPLOT. Plots the
%   leaves differently from TREEPLOT. They appear in their
%   respective layer instead of the deepest layer so that the tree
%   appears as "trimmed". The format is the same as for TREEPLOT,
%   see below. 
%
%
% TREEPLOT(p) Plot picture of the tree with parents p using default
% settings.

% TREEPLOT(p, nodes) also displays a text label defined in NODES

% TREEPLOT(p, colors) colors each edge by the RGB values in the corresponding row in COLORS 

% TREEPLOT(p, marker) plots a tree, with vertices marker properties defined
% in MARKER

%
%   See also ETREE, TREELAYOUT, ETREEPLOT.

%   Copyright (c) 1984-98 by The MathWorks, Inc.
%   $Revision: 5.8 $  $Date: 1997/11/21 23:44:59 $
%
%   Modified by RG 01-Nov-1 to use trimtreelayout instead of
%   TREELAYOUT 

%   Modified by AY 26-Aug-13 to color edges using a user-defined colormap
%   and to label nodes


% RG use another treelayout 
%[x,y,h]=treelayout(p);

SetDefaultValue('marker', 'ro');
SetDefaultValue('colors', .3*ones(numel(p), 3));

[x,y,~] = trimtreelayout(p); %(x,y) coordinates of nodes

f = find(p~=0); %ignore zero parents
pp = p(f); 
X = [x(f); x(pp); NaN(size(f))]; %each col is a pair of connected nodes
Y = [y(f); y(pp); NaN(size(f))];
X = X(:);
Y = Y(:);

plot(x,y, marker);
n = sum(~isnan(X));
X1 = X(~isnan(X));
Y1 = Y(~isnan(Y));
for i = 1:2:n
  hold on;
  plot(X1(i:i+1), Y1(i:i+1), 'Color', colors((i-1)/2+1,:));  
end

axis off
if unique(colors) > 1 %plot colorbar if multiple colors
  colorbar;
  caxis([0 1]);
end

%label nodes if they are provided
if exist('nodes', 'var')
name1 = cellstr(num2str(cell2mat(nodes')));
text(x(1,:), y(1,:), name1, 'VerticalAlignment','bottom','HorizontalAlignment','right', 'FontWeight', 'bold', 'FontSize', 14)
end
end