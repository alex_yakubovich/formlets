
function d = Cosine_Distance(u, v)
% d = Cosine_Distance(u, v) compute the cosine distance d between two
% vectors u and v of the same dimensionality.

[m1,n1] = size(u);
[m2, n2] = size(v);

if m1 < n1
  u = u';
end

if m2 < n2
  v = v';
end

d = u' * v /(norm(u) * norm(v));


