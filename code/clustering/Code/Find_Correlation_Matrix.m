
%Find_Correlation_Matrix()
% Compute the action of every formlet in the sequence

% Then find the correlation matrix by finding the cosine distance between
% every pair of formlets.

%Since we re-parameterize at each iteration, the actions are remapped.

%load('../Data/shape1')
%load('../../Regularization_Experiment/Data/formlets-lambda-0.003942-selected.mat');
%load('../../Regularization_Experiment/Data/formlets-lambda-0.003942-full.mat');

%load('../../formlets-release/data/0%-occlusion-oriented-cm-regularized-formlets-optimal-ellipse.mat')

figDir = ('../figures/');

formletRep = zeros(pref.niterations+1, pref.n, pref.nfiles);
action = zeros(pref.niterations, pref.n, pref.nfiles);
correlation = zeros(pref.niterations, pref.niterations, pref.nfiles);
M = zeros(pref.n, pref.n, pref.niterations, pref.nfiles);

for file = 1:sum(1-cellfun(@isempty,formlet_data))%pref.nfiles 
  %% read data and compute actions
  
  
  formlet_data{file}.formlets.formletRepAfterRepam{1} = formlet_data{file}.formlets.init;
  
  formletRep(1,:, file) = formlet_data{file}.formlets.formletRepAfterRepam{1}.entire; %intialization
  
  
  for k = 1:pref.niterations 
    formletRepKm1(k, :,file) = formlet_data{file}.formlets.formletRepAfterRepam{k}.entire; 
    formletRepK(k,:,file) = formlet_data{file}.formlets.formletRep{k}.entire;
    if k>1 
        M(:,:,k,file) = Repam_Matrix(formlet_data{file}.formlets.repamMapping{k})';
    end%cols sum to 1        
  end
   
  action(:,:,file) = abs(formletRepK(:,:,file) - formletRepKm1(:,:,file));  
 %%  compute correlation matrix 
  
  C = eye(32); %correlation matrix
    
  for i = 1:pref.niterations    
    Mprod = eye(pref.n); 
    
    %compute sub-diagonal part of correlation matrix
    for j = i-1:-1:1  %j<i-1
      Mprod = M(:,:, j+1,file) * Mprod; %M_i * M_{i-1}  .... * M_j                  
      ai = Mprod * action(i,:,file)'; %map ai back to aj
      aj = action(j, :,file)';
      C(i,j) = Cosine_Distance(ai,aj);      
    end
  end    
  C = C + triu(C',1); %upper-half is identical
  
  correlation(:,:,file) = C;
  
  %saveas(gcf, [figdir, 'correlation-matrix.eps'], 'epsc')
end

save('../Data/correlation-matrix-ellipse', 'correlation', 'formletRep', 'action', 'pref');

%save('../Data/correlation-matrix-full', 'correlation', 'formletRep', 'action', 'pref');

%save('../Data/correlation-matrix-selected', 'correlation', 'formletRep', 'action', 'pref');

%% plot action matrix
imagesc(action(:,:,1));
colormap(linspecer);
colorbar;

%figdir = '../Figures/';
figDir = '~/Dropbox/Thesis/Documentation/thesis-manuscript/Clustering/fig/';
xlabel 'Index on model curve';
ylabel 'Iteration';
saveas(gcf, [figDir, 'action-matrix-ellipse.eps'], 'epsc')
%saveas(gcf, [figDir, 'action-matrix.eps'], 'epsc')


%% plot correlation matrix
shape=160;
imagesc(correlation(:,:,shape));
colormap(linspecer);
colorbar;

%figdir = '../Figures/';
figDir = '~/Dropbox/Thesis/Documentation/thesis-manuscript/Clustering/fig/';
xlabel 'Iteration';
ylabel 'Iteration';
%saveas(gcf, [figDir, 'correlation-matrix.eps'], 'epsc')
set(findall(gcf,'Type','text'),'FontSize',16)
set(gca, 'FontSize', 16);
set(gcf,'renderer','opengl');

saveas(gcf, [figDir, 'correlation-matrix-shape-' num2str(shape) '.eps'], 'epsc')


formletParents = zeros(32, pref.nfiles);
for file= 1:pref.nfiles
  %partTrees{file} = Cluster_Formlets(correlation(:,:,file), 0);
  %partTrees{file} =  partTrees{file}{1};
  formletParents(:,file) = partTrees{file}.Parent;
end



